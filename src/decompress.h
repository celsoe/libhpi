#ifndef LIBHPI_DECOMPRESS_H
#define LIBHPI_DECOMPRESS_H

#include "libhpi.h"

// decompresses a given inBuffer into an outBuffer using inChunk's information
uint32_t decompress(byte *inBuffer, Chunk *inChunk, byte *outBuffer);

uint32_t decompressLZ77(byte *inBuffer, Chunk *inChunk, byte *outBuffer);

uint32_t decompressZLib(byte *inBuffer, Chunk *inChunk, byte *outBuffer);

#endif //LIBHPI_DECOMPRESS_H
