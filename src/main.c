#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>

#include "libhpi.h"
#include "packer.h"
#include "logger.h"

// FUNCTION LETTERS --------------
#define FUN_CREATE      "c"     // create a fresh new hpi file with the specified contents
#define FUN_ADD         "a"     // add files to an existing hpi file
#define FUN_EXTRACT     "x"     // extract the content of the hpi file
#define FUN_RENAME      "r"     // rename an hpi file/directory to another name
#define FUN_UPDATE      "u"     // update the file (data) of an existing hpi file
#define FUN_MOVE        "m"     // move an hpi file/directory into another hpi directory
#define FUN_DELETE      "d"     // delete files from an hpi file
#define FUN_MKDIR       "n"     // create an empty directory inside an hpi file
#define FUN_LIST        "s"     // print all the content of the hpi file
#define FUN_INFO        "i"     // print general information about an hpi file

// OPTIONS -----------------------
#define OPT_OUTDIR      "o"     // default: current directory (".")
#define OPT_TARGETDIR   "d"     // default: root ("/")
#define OPT_FILEONLY    "p"     // ignore directories and extract/add only the files; default: off
#define OPT_PERCENTAGE  "pct"   // show percentage bar; default: on
#define OPT_ENCRYPT     "enc"   // use the hpi encryption for whatever pointless reason; default: off
#define OPT_BACKUP      "b"     // backup hpi file before updating it; default: on
#define OPT_VERBOSITY   "v"     // verbosity level; default: 1 (0 = silence)
#define OPT_HPIVER      "m"     // override automatic detection for TA/TAK version, normally unecessary
#define OPT_OVERWRITE   "f"     // overwrite existing files on extraction/addition?; default: on
#define OPT_OPTIONS     "cf"    // specify path to config file; default: depends on OS
#define OPT_LISTSTYLE   "ls"    // how does the LIST function print the hpi content
#define OPT_COMPRESS    "cp"    // override whether files should be compressed; default: auto
#define OPT_COMPMODE    "cm"    // override auto compression method, zlib or lz77; default: auto

// DIAGNOSTICS -------------------
#define RET_NORMAL      0       // all went well
#define RET_WARNING     1       // somethings went wrong, but the program still finished
#define RET_ERROR       2       // a fatal error ocurred and the program was terminated earlier
#define RET_BADCMD      3       // bad command line arguments
#define RET_NOMEM       4       // no memory available for operatin
#define RET_EXITED      255     // user stopped the program with ctrl-C or something

#define OPT_YES     1
#define OPT_NO      0
#define OPT_AUTO    2

char    *optOutdir      = ".";
char    *optTargetDir   = "/";
int     optFileOnly     = OPT_YES;          // bool
int     optPercentage   = OPT_YES;          // bool
int     optEncrypt      = OPT_NO;           // bool
int     optBackup       = OPT_YES;          // bool
int     optVerbosity    = 1;
int     optHpiVer       = OPT_AUTO;
int     optOverwrite    = OPT_YES;
char    *optOptions     = "./options.txt";
int     optListStyle    = 0;
int     optCompress     = OPT_AUTO;         // bool
int     optCompMode     = OPT_AUTO;

#ifdef WIN32
    #define DIR_SEP "\\"
#else
    #define DIR_SEP "/"
#endif

//:: Utilities :://

bool parse_option_yes_no(char *arg, int *targetOption)
{
    if (strcmp(arg, "yes") == 0)
        *targetOption = OPT_YES;
    else if (strcmp(arg, "no") == 0)
        *targetOption = OPT_NO;
    else {
        testlog("Invalid argument {%s}. Should be either yes or no. Aborting...\n", arg);
        return 0; // TODO false
    }

    return 1; // TODO true
}

bool parse_option_yes_no_auto(char *arg, int *targetOption)
{
    if (strcmp(arg, "yes") == 0)
        *targetOption = OPT_YES;
    else if (strcmp(arg, "no") == 0)
        *targetOption = OPT_NO;
    else if (strcmp(arg, "auto") == 0)
        *targetOption = OPT_AUTO;
    else {
        testlog("Invalid argument {%s}. Should be either yes or no. Aborting...\n", arg);
        return 0; // TODO false
    }

    return 1; // TODO true
}

int is_file_directory(const char *path)
{
    struct stat path_stat;
    stat(path, &path_stat);
    return S_ISDIR(path_stat.st_mode);
}

char *parse_filename(char *fullpath)
{
    size_t len = strlen(fullpath);
    char *filename = fullpath;

    for (int i = 0; i < len - 1; i++) {
        if (fullpath[i] == '/') {
            filename = fullpath + i + 1;
        }
    }

    return filename;
}

int directory_exists(char *path)
{
    DIR *dir = opendir(path);
    if (dir) {
        closedir(dir);
    } else {
        return 0;
    }

    return 1;
}

//:: HPI Procedures :://
// The hpi references must already be loaded and is not the responsability of these functions

int insert_file(char *filepath, HPI_2 *hpi, HPI_Item_2 *parent)
{
    char *filename = parse_filename(filepath);
    FILE *inputFile = fopen(filepath, "r");

    if (inputFile == NULL) {
        // TODO error opening file, skipping it...
        fclose(inputFile);
        return RET_WARNING;
    }

    if (fseek(inputFile, 0L, SEEK_END) != 0) {
        // TODO unknown error
        fclose(inputFile);
        return RET_WARNING;
    }

    long bufSize = ftell(inputFile);
    if (bufSize == -1) {
        // TODO unknown error
        fclose(inputFile);
        return RET_WARNING;
    }

    if (fseek(inputFile, 0L, SEEK_SET) != 0) {
        // TODO unknown error
        fclose(inputFile);
        return RET_WARNING;
    }

    char *data = malloc(sizeof(char) * (bufSize + 1));
    size_t dataLen = fread(data, sizeof(char), bufSize, inputFile);

    if (ferror(inputFile) != 0) {
        // TODO Error reading file data
        fclose(inputFile);
        return RET_WARNING;
    }

    fclose(inputFile);

    testlog("Inserting: %s\n", filepath);

    char *pathname = malloc(strlen(parent->path) + strlen(filename) + 1);
    strcpy(pathname, parent->path);
    strcat(pathname, filename);
    HPI_Item_2 *existing = find_item_2(hpi->rootItem, pathname, true);
    free(pathname);

    if (existing != NULL) {
        if (OPT_OVERWRITE) {
            puts("(The file already exists, overwriting it...)");
            update_file_2(hpi, data, dataLen, existing);
        }
        else {
            puts("(The already exists, skipping it...)");
        }
    }
    else {
        add_file_2(hpi, data, dataLen, filename, parent);
    }

    // TODO free(data);

    return 0;
}

int insert_directory(char *filepath, HPI_2 *hpi, HPI_Item_2 *parent)
{
    char *filename = parse_filename(filepath);
    HPI_Item_2 *item = make_dir_2(hpi, filename, parent);

    if (item == NULL) {
        // TODO Couldn't create item for directory, skipping it...
        return RET_WARNING;
    }

    struct dirent *de;
    DIR *dir = opendir(filepath);

    if (dir == NULL) {
        // TODO Couldn't open directory, skipping it...
        return RET_WARNING;
    }

    while ((de = readdir(dir)) != NULL) {
        if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0) {
            continue;
        }

        char *fullpath = malloc(strlen(filepath) + 1 + strlen(de->d_name)); // filepath + / + name
        sprintf(fullpath, "%s/%s", filepath, de->d_name);

        if (is_file_directory(fullpath)) {
            insert_directory(fullpath, hpi, item);
        } else {
            insert_file(fullpath, hpi, item);
        }

        free(fullpath);
    }

    closedir(dir);
    return 0;
}

int extract_item(FILE *file, HPI_Item_2 *item, char *parentPath)
{
    char *outpath = malloc(255); // TODO dynamic size
    strcpy(outpath, parentPath);
    if (strcmp(item->name, "") != 0) {
        size_t sepLen = strlen(DIR_SEP);
        size_t curLen = strlen(parentPath);
        if (curLen >= sepLen && strcmp(parentPath + curLen - sepLen, DIR_SEP) != 0) {
            strcat(outpath, DIR_SEP);
        }
        strcat(outpath, item->name);
    }

    testlog("Extracting {%s}\n", outpath);

    if (item->isDirectory) {
        if (!directory_exists(outpath)) {
            int nError = 0;
            #if defined(_WIN32)
                nError = _mkdir(outpath);
            #else
                nError = mkdir(outpath, 0777);
            #endif
            if (nError != 0) {
                free(outpath);
                testlog("Failed to create a directory, skipping it: %s\n", outpath);
                return RET_WARNING;
            }
        }

        HPI_Item_2 *nextSibling = item->firstChild;
        while (nextSibling != NULL) {
            extract_item(file, nextSibling, outpath);
            nextSibling = nextSibling->nextSibling;
        }

        free(outpath);
    }
    else {
        byte *buffer;
        uint32_t outlen = extract_to_mem_2(file, item, &buffer);

        if (outlen) {
            FILE *outfile = fopen(outpath, "wb");
            free(outpath);

            if (outfile) {
                fwrite(buffer, outlen - 1, 1, outfile);
                free(buffer);
                fclose(outfile);
            }
            else {
                // TODO error...
                free(buffer);
                fclose(outfile);
                return RET_WARNING;
            }
        }
        else {
            // TODO error...
            // free(buffer); // no allocation happened when outlen == 0
            return RET_WARNING;
        }
    }

    return 0;
}

//:: Main Functions :://

int do_create(char *targetHpi, char* inputFiles[], int inputCount)
{
    // TODO create an HPI from scratch
}

int do_add(char *targetHpi, char* inputFiles[], int inputCount)
{
    if (inputCount == 0) {
        puts("No input files specified. Exiting");
        return RET_BADCMD;
    }

    HPI_2 hpi;
    Version version;
    FILE *file = fopen(targetHpi, "rw+");

    load_version(file, &version);

    if (version.marker != HAPI_MARKER || version.version != VERSION_TAK) {
        // TODO puts...
        return RET_ERROR;
    }

    load_2(file, &hpi);

    HPI_Item_2 *parent = NULL;

    if (strcmp(optTargetDir, "/") == 0) {
        parent = hpi.rootItem;
    } else {
        parent = find_item_2(hpi.rootItem, optTargetDir, true);
    }

    if (parent != NULL) {
        for (int i = 0; i < inputCount; i++) {
            char *nextInput = inputFiles[i];

            if (is_file_directory(nextInput)) {
                insert_directory(nextInput, &hpi, parent);
            } else {
                insert_file(nextInput, &hpi, parent);
            }
        }
    } else {
        testlog("Error. The speficied target directory wasn't found: %s\n", optTargetDir);
    }

    clean_2(&hpi);

    fclose(file);
}

int do_mkdir(char *targetHpi, char* inputFiles[], int inputCount)
{
    if (inputCount == 0) {
        puts("No input files specified. Exiting");
        return RET_BADCMD;
    }

    HPI_2 hpi;
    Version version;
    FILE *file = fopen(targetHpi, "rw+");

    load_version(file, &version);

    if (version.marker != HAPI_MARKER || version.version != VERSION_TAK) {
        // TODO puts...
        return RET_ERROR;
    }

    load_2(file, &hpi);

    HPI_Item_2 *parent = NULL;

    if (strcmp(optTargetDir, "/") == 0) {
        parent = hpi.rootItem;
    } else {
        parent = find_item_2(hpi.rootItem, optTargetDir, true);
    }

    if (parent != NULL) {
        for (int i = 0; i < inputCount; i++) {
            char *nextInput = inputFiles[i];
            // TODO check if a file with the same name doesn't already exist in the hpi
            // if it already exists, skip it (can't override a directory...)

            make_dir_2(&hpi, nextInput, parent);
        }
    } else {
        testlog("Error. The speficied target directory wasn't found: %s\n", optTargetDir);
    }

    clean_2(&hpi);

    fclose(file);
}

int do_extract(char *targetHpi, char* inputFiles[], int inputCount)
{
    if (!directory_exists(optOutdir)) {
        testlog("Failed to open target directory, please make sure it exists: %s\n", optOutdir);
        return RET_ERROR;
    }

    HPI_2 hpi;
    Version version;
    FILE *file = fopen(targetHpi, "r");

    load_version(file, &version);

    if (version.marker != HAPI_MARKER || version.version != VERSION_TAK) {
        // TODO puts...
        return RET_ERROR;
    }

    load_2(file, &hpi);

    if (inputCount == 0) {
        extract_item(file, hpi.rootItem, optOutdir);
    }
    else {
        for (int i = 0; i < inputCount; i++) {
            char *nextInput = inputFiles[i];
            HPI_Item_2 *nextItem = find_item_2(hpi.rootItem, nextInput, false);

            if (nextItem == NULL) {
                testlog("The specified file wasn't found in the hpi: %s\nSkipping it...\n", nextInput);
                continue;
            }

            extract_item(file, nextItem, optOutdir);
        }
    }

    clean_2(&hpi);
    fclose(file);
}

int do_delete(char *targetHpi, char* inputFiles[], int inputCount)
{
    HPI_2 hpi;
    Version version;
    FILE *file = fopen(targetHpi, "rw+");

    load_version(file, &version);

    if (version.marker != HAPI_MARKER || version.version != VERSION_TAK) {
        // TODO puts...
        return RET_ERROR;
    }

    load_2(file, &hpi);

    for (int i = 0; i < inputCount; i++) {
        char *nextInput = inputFiles[i];
        HPI_Item_2 *nextItem = find_item_2(hpi.rootItem, nextInput, false);

        if (nextItem == NULL) {
            testlog("The specified file wasn't found in the hpi: %s\nSkipping it...\n", nextInput);
            continue;
        }

        delete_auto_2(&hpi, nextItem);
    }

    clean_2(&hpi);
    fclose(file);
}

int do_rename(char *targetHpi, char* inputFiles[], int inputCount)
{
    if (inputCount != 2) {
        puts("Error. The rename operation should receive exactly 2 input arguments: <path_to_hpi_file> <new_name>");
        return RET_BADCMD;
    }

    HPI_2 hpi;
    Version version;
    FILE *file = fopen(targetHpi, "rw+");

    load_version(file, &version);

    if (version.marker != HAPI_MARKER || version.version != VERSION_TAK) {
        // TODO puts...
        return RET_ERROR;
    }

    load_2(file, &hpi);

    int ret = RET_NORMAL;
    char *originalName = inputFiles[0];
    char *renamedName = inputFiles[1];

    HPI_Item_2 *target = find_item_2(hpi.rootItem, originalName, false);

    if (target != NULL) {
        HPI_Item_2 *exists = find_item_2(target->parent, renamedName, false);

        if (exists == NULL) {
            rename_2(&hpi, target, renamedName);
        }
        else {
            ret = RET_ERROR;
            testlog("Error. The desired name already exists in the same directory of the hpi file.\n");
        }
    }
    else {
        ret = RET_ERROR;
        testlog("Couldn't find the file in the hpi: %s\n", originalName);
    }

    clean_2(&hpi);
    fclose(file);

    return ret;
}

int do_move(char *targetHpi, char* inputFiles[], int inputCount)
{
    if (inputCount != 2) {
        puts("Error. The move operation should receive exactly 2 input arguments: <item_to_move_name> <target_directory_name>");
        return RET_BADCMD;
    }

    HPI_2 hpi;
    Version version;
    FILE *file = fopen(targetHpi, "rw+");

    load_version(file, &version);

    if (version.marker != HAPI_MARKER || version.version != VERSION_TAK) {
        // TODO puts...
        return RET_ERROR;
    }

    load_2(file, &hpi);

    int ret = RET_NORMAL;
    HPI_Item_2 *item = find_item_2(hpi.rootItem, inputFiles[0], false);
    HPI_Item_2 *newParent = find_item_2(hpi.rootItem, inputFiles[1], false);

    if (item && newParent) {
        move_item_2(&hpi, item, newParent);
    }
    else {
        puts("Couldn't find the item of the input names");
        ret = RET_ERROR;
    }

    clean_2(&hpi);
    fclose(file);

    return RET_NORMAL;
}

int do_list(char *targetHpi, char* inputFiles[], int inputCount)
{
    HPI_2 hpi;
    Version version;
    FILE *file = fopen(targetHpi, "r");

    load_version(file, &version);

    if (version.marker != HAPI_MARKER || version.version != VERSION_TAK) {
        // TODO puts...
        return RET_ERROR;
    }

    load_2(file, &hpi);

    print_tree_2(hpi.rootItem);

    clean_2(&hpi);

    fclose(file);
}

int do_info(char *targetHpi, char* inputFiles[], int inputCount)
{
    HPI_2 hpi;
    Version version;
    FILE *file = fopen(targetHpi, "r");

    load_version(file, &version);

    if (version.marker != HAPI_MARKER || version.version != VERSION_TAK) {
        // TODO puts...
        return RET_ERROR;
    }

    load_2(file, &hpi);

    uint32_t fileCount = calc_tree_size_2(hpi.rootItem, 0, COUNT_ITENS_ENTRY);
    uint32_t dirCount = calc_tree_size_2(hpi.rootItem, 0, COUNT_ITENS_DIR);

    testlog("Marker: 0x%02x (%s)\n", version.marker, version.marker == HAPI_MARKER ? "Valid" : "Invalid");

    testlog("Version: %s\n", (
        version.version == VERSION_TAK ? "2 (TA:K)" :
        version.version == VERSION_TA ? "1 (TA)" :
        "Unknown Version" // impossible to reach here
    ));

    testlog("File Count: %d\n", fileCount);
    testlog("Directory Count: %d\n", dirCount);

    // TODO print more information I guess

    clean_2(&hpi);

    fclose(file);
}

//:: Main :://

int main(int argc, char *argv[])
{
    char *mode = NULL;
    char *targetHpi = NULL;

    char *options[argc];
    char *arguments[argc];
    int opCount = 0;
    char *lastOp = NULL;

    char *inputFiles[argc];
    int inputCount = 0;

    for (int i = 1; i < argc; i++) { // ignore argv[0]
        char *nextArg = argv[i];
        int len = strlen(nextArg);

        if (i == 1) {
            if (strcmp(nextArg, FUN_CREATE) == 0) {
                mode = FUN_CREATE;
            } else if (strcmp(nextArg, FUN_ADD) == 0) {
                mode = FUN_ADD;
            } else if (strcmp(nextArg, FUN_EXTRACT) == 0) {
                mode = FUN_EXTRACT;
            } else if (strcmp(nextArg, FUN_LIST) == 0) {
                mode = FUN_LIST;
            } else if (strcmp(nextArg, FUN_DELETE) == 0) {
                mode = FUN_DELETE;
            } else if (strcmp(nextArg, FUN_MKDIR) == 0) {
                mode = FUN_MKDIR;
            } else if (strcmp(nextArg, FUN_INFO) == 0) {
                mode = FUN_INFO;
            } else if (strcmp(nextArg, FUN_RENAME) == 0) {
                mode = FUN_RENAME;
            } else if (strcmp(nextArg, FUN_MOVE) == 0) {
                mode = FUN_MOVE;
            } else {
                puts("The first argument MUST be an operation mode. Aborting...");
                return RET_BADCMD;
            }

            continue;
        }

        if (len >= 2 && nextArg[0] == '-') {
            char *opt = nextArg + 1;
            testlog("{%s} is a parameter\n", opt); //

            if (lastOp != NULL) {
                puts("Can't have a parameter followed by another parameter");
                return RET_BADCMD;
            }

            lastOp = opt;
            options[opCount] = opt;
            continue;
        }

        if (lastOp != NULL) {
            testlog("{%s} is an argument to {%s}\n", nextArg, lastOp); //
            arguments[opCount] = nextArg;
            opCount++;
            lastOp = NULL;
            continue;
        }

        if (i == 2) {
            targetHpi = nextArg; // TODO validate this file later
            continue;
        }

        inputFiles[inputCount] = nextArg;
        inputCount++;
    }

    if (lastOp != NULL) {
        testlog("Parameter {%s} is missing its argument. Aborting...\n", lastOp);
        return RET_BADCMD;
    }

    if (targetHpi == NULL) {
        puts("An HPI file must be defined directly after the operation option. Aborting...\n");
        return RET_BADCMD;
    }

    if (targetHpi == NULL && inputCount == 0) {
        puts("No input file supplied. Aborting...");
        return RET_BADCMD;
    }

    puts("---\nAnd the verdict is...\n"); //

    testlog("Target: \n%s\n", targetHpi);

    testlog("Operation: \n%s\n", mode);

    puts("Input files:"); //

    for (int i = 0; i < inputCount; i++) { //
        testlog("{%s} ", inputFiles[i]); //
    } //
    puts(""); //

    puts("Parameter options:"); //

    for (int i = 0; i < opCount; i++) {
        char *par = options[i];
        char *arg = arguments[i];
        testlog("{%s} = {%s}\n", par, arg); //

        if (strcmp(par, OPT_OUTDIR) == 0) {
            optOutdir = arg;
        } else if (strcmp(par, OPT_TARGETDIR) == 0) {
            optTargetDir = arg;
            if (optTargetDir[0] != '/') {
                testlog("Warning. The specified target directory (%s) should start with a '/'\n", optTargetDir);
            }
        } else if (strcmp(par, OPT_FILEONLY) == 0) {
            if (!parse_option_yes_no(arg, &optFileOnly))
                return RET_BADCMD;
        } else if (strcmp(par, OPT_PERCENTAGE) == 0) {
            if (!parse_option_yes_no(arg, &optPercentage))
                return RET_BADCMD;
        } else if (strcmp(par, OPT_ENCRYPT) == 0) {
            if (!parse_option_yes_no(arg, &optEncrypt))
                return RET_BADCMD;
        } else if (strcmp(par, OPT_BACKUP) == 0) {
            if (!parse_option_yes_no(arg, &optBackup))
                return RET_BADCMD;
        } else if (strcmp(par, OPT_VERBOSITY) == 0) {
            optVerbosity = (int) arg[0];
        } else if (strcmp(par, OPT_HPIVER) == 0) {
            // TODO
        } else if (strcmp(par, OPT_OVERWRITE) == 0) {
            if (!parse_option_yes_no(arg, &optOverwrite))
                return RET_BADCMD;
        } else if (strcmp(par, OPT_OPTIONS) == 0) {
            optOptions = arg;
        } else if (strcmp(par, OPT_LISTSTYLE) == 0) {
            optListStyle = (int) arg[0];
        } else if (strcmp(par, OPT_COMPRESS) == 0) {
            if (!parse_option_yes_no_auto(arg, &optCompress))
                return RET_BADCMD;
        } else if (strcmp(par, OPT_COMPMODE) == 0) {
            // TODO
        } else {
            testlog("{%s} is an invalid parameter. Aborting...\n", par);
            return RET_BADCMD;
        }
    }

    puts("--- Commence operation ---\n");

    if (mode == FUN_CREATE) {
        return do_create(targetHpi, inputFiles, inputCount);
    } else if (mode == FUN_ADD) {
        return do_add(targetHpi, inputFiles, inputCount);
    } else if (mode == FUN_EXTRACT) {
        return do_extract(targetHpi, inputFiles, inputCount);
    } else if (mode == FUN_LIST) {
        return do_list(targetHpi, inputFiles, inputCount);
    } else if (mode == FUN_DELETE) {
        return do_delete(targetHpi, inputFiles, inputCount);
    } else if (mode == FUN_MKDIR) {
        return do_mkdir(targetHpi, inputFiles, inputCount);
    } else if (mode == FUN_INFO) {
        return do_info(targetHpi, inputFiles, inputCount);
    } else if (mode == FUN_RENAME) {
        return do_rename(targetHpi, inputFiles, inputCount);
    } else if (mode == FUN_MOVE) {
        return do_move(targetHpi, inputFiles, inputCount);
    } else {
        // impossible to reach here
    }
}
