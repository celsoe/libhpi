#ifndef LIBHPI_COMPRESS_H
#define LIBHPI_COMPRESS_H

#include "libhpi.h"

// compresses a given inBuffer into an outBuffer using inChunk's information
// reads ioChunk->decompressedSize to know the length of inBuffer
// also reads ioChunk->isEncrypted to know if it should apply encryption
// writes the ioChunk->checksum with the manual encryption checksum
// whoever calls this function is responsible for freeing *outBuffer
// returns compressed size (aka length of outBuffer)
uint32_t recompress(byte *inBuffer, Chunk *ioChunk, byte **outBuffer);

// returns compressed size
uint32_t compressLZ77(byte *inBuffer, uint32_t len, byte **outBuffer);

// returns compressed size
uint32_t compressZLib(byte *inBuffer, uint32_t len, byte **outBuffer);

#endif //LIBHPI_COMPRESS_H
