#include "logger.h"
#include <stdarg.h>
#include <stdio.h>

#define TESTLOGSTREAM stdout;

int testlog(const char *fmt, ...)
{
    FILE *stream = TESTLOGSTREAM;

    va_list args;
    va_start(args, fmt);
    int rc = vfprintf(stream, fmt, args);
    va_end(args);
    return rc;
}
