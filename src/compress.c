#include <stdlib.h>

#include "compress.h"
#include "logger.h"
#include "zlib.h"

uint32_t recompress(byte *inBuffer, Chunk *ioChunk, byte **outBufferPtr)
{
    uint32_t outSize;

    switch (ioChunk->compMethod) {
        case 1 : outSize = compressLZ77(inBuffer, ioChunk->decompressedSize, outBufferPtr); break;
        case 2 : outSize = compressZLib(inBuffer, ioChunk->decompressedSize, outBufferPtr); break;
        default : outSize = 0;
    }

    testlog("outSize: %u\n", outSize);

    if (outSize == 0) {
        puts("\x1B[31mCompression failed!\x1B[0m");
        return 0;
    }

    byte n;

    for (uint32_t x = 0; x < outSize; x++) {
        n = (unsigned char) (*outBufferPtr)[x];
        if (ioChunk->isEncrypted) {
            n = (n ^ x) + x;
            (*outBufferPtr)[x] = n;
        }

        ioChunk->checksum += n;
    }


    return outSize;
}

uint32_t compressLZ77(byte *inBuffer, uint32_t len, byte **outBuffer)
{
    // TODO
}

uint32_t compressZLib(byte *inBuffer, uint32_t len, byte **outBuffer)
{
    z_stream zs;
    int result;
    char *out = malloc(len * 2); // 131072

    /*puts("---");
    for (uint32_t i = 0; i < len; i++) {
        testlog("0x%02x ", inBuffer[i]);
    }
    puts("\n---");*/

    zs.next_in = inBuffer;
    zs.avail_in = len;
    zs.total_in = 0;

    zs.next_out = out;
    zs.avail_out = len * 2;
    zs.total_out = 0;

    zs.msg = NULL;
    zs.state = NULL;
    zs.zalloc = Z_NULL;
    zs.zfree = Z_NULL;
    zs.opaque = Z_NULL;

    zs.data_type = Z_BINARY;
    zs.adler = 0;
    zs.reserved = 0;

    result = deflateInit(&zs, Z_DEFAULT_COMPRESSION);
    if (result != Z_OK) {
        testlog("Error on deflateInit %d\nMessage: %s\n", result, zs.msg);
        return 0;
    }

    result = deflate(&zs, Z_FINISH);
    if (result != Z_STREAM_END) {
        testlog("Error on deflate %d\nMessage: %s\n", result, zs.msg);
        return 0;
    }

    result = deflateEnd(&zs);
    if (result != Z_OK) {
        testlog("Error on deflateEnd %d\nMessage: %s\n", result, zs.msg);
        return 0;
    }

    *outBuffer = out;
    return zs.total_out;
}
