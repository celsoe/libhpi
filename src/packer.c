#include "packer.h"

#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdbool.h>
#include <time.h>

#include "string.h"
#include "assert.h"
#include "logger.h"

#include "compress.h"

uint32_t first_positional_item_2(HPI_Item_2 *item, uint32_t smallest)
{
    if (item->nextSibling != NULL) {
        uint32_t result = first_positional_item_2(item->nextSibling, smallest);

        if (result != 0 && (result < smallest || smallest == 0))
            smallest = result;
    }

    if (item->firstChild != NULL) {
        uint32_t result = first_positional_item_2(item->firstChild, smallest);

        if (result != 0 && (result < smallest || smallest == 0))
            smallest = result;
    }

    if (item->isDirectory)
        return smallest;

    // testlog("%d\t%s\n", item->entry->dataStartPtr, item->path);
    return item->entry->dataStartPtr;
}

uint32_t last_positional_item_2(HPI_Item_2 *item, uint32_t largest)
{
    if (item->nextSibling != NULL) {
        uint32_t result = last_positional_item_2(item->nextSibling, largest);

        if (result != 0 && (result > largest))
            largest = result;
    }

    if (item->firstChild != NULL) {
        uint32_t result = last_positional_item_2(item->firstChild, largest);

        if (result != 0 && (result > largest))
            largest = result;
    }

    if (item->isDirectory)
        return largest;

    uint32_t newLargest = item->entry->dataStartPtr
        + (item->entry->compressedSize ? item->entry->compressedSize : item->entry->decompressedSize);

    return newLargest > largest ? newLargest : largest; // Math.max
}

void check_order_2(HPI_2 *hpi, char *first, char *second, char *third)
{
    uint32_t nameSection = hpi->header.namesBlockPtr;
    uint32_t dirSection = hpi->header.dirBlockPtr;
    uint32_t fileSection = first_positional_item_2(hpi->rootItem, 0);
    // testlog("N:%u D:%u F:%u\n", nameSection, dirSection, fileSection);

    uint32_t order[3] = {nameSection, dirSection, fileSection};
    for (int i = 0; i < 3; i++) {
        for (int j = i + 1; j < 3; j++) {
            if (order[i] > order[j]) {
                uint32_t temp = order[i];
                order[i] = order[j];
                order[j] = temp;
            }
        }
    }

    char result[3];
    for (int i = 0; i < 3; i++) {
        if (order[i] == nameSection)
            result[i] = 'N';
        else if (order[i] == dirSection)
            result[i] = 'D';
        else
            result[i] = 'F';
    }

    *first  = result[0];
    *second = result[1];
    *third  = result[2];
}

int assert_order_2(HPI_2 *hpi, char x1, char x2, char x3) {
    char a, b, c;
    check_order_2(hpi, &a, &b, &c);

    if (a != x1 || b != x2 || c != x3) {
        testlog("HPI sections are out of supported order, aborting operation!\nFile order: [%c + %c + %c] , "
                "Expected order: [%c + %c + %c]\n", a, b, c, x1, x2, x3);

        return -1;
    }

    return 0;
}

// replaces the hpi->namesBuff and returns the length difference between the new and old name buffer
// name must be null-terminated string
int32_t add_name_to_buffer_2(HPI_2 *hpi, char *name)
{
    assert(name[strlen(name)] == '\0'); // TODO ERROR not a null-terminated string

    uint32_t nameLen = strlen(name) + 1; // + 1 for null terminator
    uint32_t oldLen = hpi->namesBuffLen;
    uint32_t newLen = oldLen + nameLen;

    char *oldBuffer = hpi->namesBuff;
    char *newBuffer = malloc(newLen);

    // newBuffer = oldBuffer + name
    memcpy(newBuffer, oldBuffer, oldLen);
    memcpy(newBuffer + oldLen, name, nameLen); // + 1 for null terminator

    for (uint32_t i = 0; i < newLen; i++) {
        // if (i < oldLen) {
        //     newBuffer[i] = oldBuffer[i];
        // } else {
        //     newBuffer[i] = name[i - oldLen];
        // }

        // testlog("%02u: 0x%02x %c\n", i, newBuffer[i], newBuffer[i]);
    }

    free(oldBuffer);
    hpi->namesBuff = newBuffer;
    hpi->namesBuffLen = newLen;

    return newLen - oldLen; // AKA nameLen... lol
}

int32_t delete_name_from_buffer(HPI_2 *hpi, uint32_t namePtr)
{
    char *name = hpi->namesBuff + namePtr;
    uint32_t nameLen = strlen(name) + 1; // + 1 for null terminator
    uint32_t oldLen = hpi->namesBuffLen;
    uint32_t newLen = oldLen - nameLen;

    testlog("Deleting name: %s (%u)\n", name, nameLen);

    char *oldBuffer = hpi->namesBuff;
    char *newBuffer = malloc(newLen);

    uint32_t lenPre = namePtr; // length of chars before namePtr
    uint32_t lenPos = oldLen - namePtr - nameLen; // length of chars after namePtr

    if (lenPre > 0) {
        memcpy(newBuffer, oldBuffer, lenPre);
    }
    if (lenPos > 0) {
        memcpy(newBuffer + lenPre, oldBuffer + namePtr + nameLen, lenPos);
    }

    testlog("OldLen: %u; NewLen: %u\n", oldLen, newLen);
    testlog("NamePtr: %u; NameLen: %u\n", namePtr, nameLen);
    testlog("LenPre: %u; LenPos: %u\n", lenPre, lenPos);
    puts("Printing oldBuffer:");
    fwrite(oldBuffer, oldLen, 1, stdout); puts("");
    puts("Printing newBuffer:");
    fwrite(newBuffer, newLen, 1, stdout); puts("");

    free(oldBuffer);
    hpi->namesBuff = newBuffer;
    hpi->namesBuffLen = newLen;

    return newLen - oldLen; // AKA -nameLen
}

// updates the namePtr to conform to a new size variation for pointers that
// are to the right of the specified minimum pointer
void update_name_pointers(HPI_Item_2 *item, uint32_t minPos, int32_t delta)
{
    if (item->firstChild != NULL) {
        update_name_pointers(item->firstChild, minPos, delta);
    }

    if (item->nextSibling != NULL) {
        update_name_pointers(item->nextSibling, minPos, delta);
    }

    uint32_t *targetPtr;
    if (item->isDirectory) {
        targetPtr = &item->dir->namePtr;
    }
    else {
        targetPtr = &item->entry->namePtr;
    }

    if (*targetPtr < minPos) {
        return;
    }

    if ((int32_t) *targetPtr + delta < 0) {
        // TODO error. probably uneeded, this should be impossible
        return;
    }

    *targetPtr += delta;
}

// updates the dataStartPtr to conform to a new size variation for pointers that
// are to the right of the specified minimum pointer
void update_file_pointers(HPI_Item_2 *item, uint32_t minPos, int32_t delta)
{
    if (item->firstChild != NULL) {
        update_file_pointers(item->firstChild, minPos, delta);
    }

    if (item->nextSibling != NULL) {
        update_file_pointers(item->nextSibling, minPos, delta);
    }

    if (item->isDirectory) {
        return;
    }

    uint32_t *targetPtr = &item->entry->dataStartPtr;

    if (*targetPtr < minPos) {
        return;
    }

    if ((int32_t) *targetPtr + delta < 0) {
        // TODO error. probably uneeded, this should be impossible
        return;
    }

    *targetPtr += delta;
}

// Directory rebuilding =================================================================

// reset all dir firstSubDirPtr and firstFilePtr to 0
void reset_dir_tree_2(HPI_Item_2 *root)
{
    root->origin = 0;

    if (root->isDirectory) {
        root->dir->firstSubDirPtr = 0;
        root->dir->firstFilePtr = 0;
    }

    if (root->firstChild != NULL) {
        reset_dir_tree_2(root->firstChild);
    }

    if (root->nextSibling != NULL) {
        reset_dir_tree_2(root->nextSibling);
    }
}

void write_dir_next_2(HPI_Item_2 *dirItem, uint32_t bufferPos, bool dirsPass)
{
    dirItem->origin = (void *) (long) bufferPos;

    if (dirItem->parent != NULL) { // always true unless it's the hpi root
        uint32_t *ptr = dirsPass ? &dirItem->parent->dir->firstSubDirPtr : &dirItem->parent->dir->firstFilePtr;
        if (*ptr == 0) { // if this is the first child of parent
            *ptr = (long) dirItem->origin;
            /*testlog("%s%s%s's first %s points to %u (count: %u)%s\n",
                KRED, dirItem->parent->path, KBLU, dirsPass ? "DIR" : "FILE", *ptr,
                dirsPass ? dirItem->parent->dir->subDirCount : dirItem->parent->dir->fileCount, KNRM);*/
        }
    }

    // if (dirsPass)
    /*testlog("%swrite_dir_next_2 : %s%s%s at %s0x%02x%s (%u)%s\n", KGRN, KRED, dirItem->path, KGRN,
        KRED, bufferPos, KGRN, bufferPos, KNRM);*/
}

uint32_t rebuild_dir_next_2(HPI_Item_2 *item, uint32_t bufferPos, bool dirsPass)
{
    if (item->isDirectory == dirsPass) {
        write_dir_next_2(item, bufferPos, dirsPass);
        bufferPos += dirsPass ? sizeof(Directory_2) : sizeof(Entry_2);
    }

    // first write every sibling...
    /*HPI_Item_2 *next = item->nextSibling;
    while (next != NULL) {
        if (next->isDirectory == dirsPass) {
            write_dir_next_2(next, bufferPos, dirsPass);
            bufferPos += dirsPass ? sizeof(Directory_2) : sizeof(Entry_2);
        }

        next = next->nextSibling;
    }*/

    // ... then siblings
    if (item->nextSibling != NULL) {
        bufferPos = rebuild_dir_next_2(item->nextSibling, bufferPos, dirsPass);
    }

    // ...then children
    if (item->firstChild != NULL) {
        bufferPos = rebuild_dir_next_2(item->firstChild, bufferPos, dirsPass);
        // TODO also rebuild the children's siblings too!
    }

    return bufferPos;
}

void rebuild_dir_commit_2(HPI_Item_2 *nextItem, byte *outBuffer)
{
    assert(nextItem->origin != 0 || nextItem->parent == NULL); // origin cant be 0, unless it's the rootItem

    void *buf = nextItem->isDirectory ?
        (void *) nextItem->dir :
        (void *) nextItem->entry;
    size_t len = nextItem->isDirectory ? sizeof(Directory_2) : sizeof(Entry_2);
    uint32_t bufferPos = (long) nextItem->origin;

    // testlog("%sWriting [%s] @0x%02x (%u)%s\n", KYEL, nextItem->name, nextItem->origin, nextItem->origin, KNRM);
    memcpy(outBuffer + bufferPos, buf, len); // write to outbuffer

    if (nextItem->isDirectory) {
        nextItem->dir = (Directory_2 *) (outBuffer + bufferPos);
    } else {
        nextItem->entry = (Entry_2 *) (outBuffer + bufferPos);
    }

    if (nextItem->firstChild != NULL) {
        rebuild_dir_commit_2(nextItem->firstChild, outBuffer);
    }

    if (nextItem->nextSibling != NULL) {
        rebuild_dir_commit_2(nextItem->nextSibling, outBuffer);
    }
}

int32_t rebuild_dir_buffer_2(HPI_2 *hpi)
{
    uint32_t oldLen = hpi->dirBuffLen;
    uint32_t newLen = calc_tree_size_2(hpi->rootItem, 0, COUNT_BYTES_ANY); // expected size

    byte *newBuffer = malloc(newLen);

    assert(newBuffer != NULL);

    reset_dir_tree_2(hpi->rootItem);

    uint32_t actualNewLen = rebuild_dir_next_2(hpi->rootItem, 0, true);
    actualNewLen = rebuild_dir_next_2(hpi->rootItem, actualNewLen, false);
    rebuild_dir_commit_2(hpi->rootItem, newBuffer);

    testlog("DirBuffer Sizes:\n\t(expected ==> got)\n\t%u ==> %u\n", newLen, actualNewLen);
    // write_buffer("./rebuiltDir.bin", newBuffer, actualNewLen);
    // return 0;
    assert(actualNewLen == newLen);

    free(hpi->directoryBuff);
    hpi->directoryBuff = newBuffer;
    hpi->dirBuffLen = newLen;
}

// Main operations ======================================================================

void add_file_2(HPI_2 *hpi, char *data, uint32_t dataSize, char *name, HPI_Item_2 *parent)
{
    assert(parent->isDirectory);

    // fileSection actually refers to the END of the file section,
    // while the 2 other sections below, refer to their STARTs.
    // fileSection and nameSection are currently redundant...
    // nameSection is being updated to the new start but
    // fileSection isn't being updated to the new end, btw
    uint32_t fileSection = last_positional_item_2(hpi->rootItem, 0);
    uint32_t nameSection = hpi->header.namesBlockPtr;
    uint32_t dirSection = hpi->header.dirBlockPtr;
    testlog("F:%d N:%d D:%d\n", fileSection, nameSection, dirSection);

    if (assert_order_2(hpi, 'F', 'N', 'D')) {
        return;
    }

    uint32_t filesIncrement = 0; // < refers to the files data section, not from the whole FILE
    uint32_t namesIncrement = 0; // never used in the current model
    uint32_t dirIncrement = 0; // never used in the current model

    // --> create a name and add it to nameBuff

    uint32_t newNamePtr = hpi->namesBuffLen;
    int32_t namesPosDelta = add_name_to_buffer_2(hpi, name);
    assert(namesPosDelta >= 0);

    namesIncrement = (uint32_t) namesPosDelta;
    // dirSection += namesIncrement; // NOPE, dirSection += COMPRESSED(namesBuff)'s increment

    // --> create file entry and compress file data

    Entry_2 *entry = malloc(sizeof(Entry_2));
    entry->namePtr = newNamePtr;
    entry->dataStartPtr = fileSection;
    entry->decompressedSize = dataSize;  // raw data size
    entry->compressedSize = 0;           // null init
    entry->date = time(NULL);
    entry->checksum = 0;

    byte *outBlock = NULL;
    Chunk *outChunk = NULL;
    uint32_t didCompress = compress_file_2(data, dataSize, entry, &outBlock, &outChunk);

    if (didCompress/*outChunk && outBlock*/) {
        filesIncrement = sizeof(Chunk) + outChunk->compressedSize;
        entry->compressedSize = filesIncrement;
    }
    else {
        filesIncrement = entry->decompressedSize; // aka dataSize
    }

    nameSection += filesIncrement;
    dirSection += filesIncrement;

    // --> create an item and insert into the parent directory

    HPI_Item_2 *item;
    item = malloc(sizeof(HPI_Item_2));
    item->name = malloc(sizeof(name)); strcpy(item->name, name);
    item->path = NULL;
    // item->size = didCompress ? entry->compressedSize : entry->decompressedSize;
    item->size = entry->decompressedSize; // seems like it should always refer to the decompressed size
    item->isDirectory = 0;
    item->dir = NULL;
    item->entry = entry;
    item->parent = parent;
    item->nextSibling = NULL;
    item->firstChild = NULL;
    item->origin = 0; // null init

    char *myPath = malloc(255/*sizeof(name) + sizeof(currentPath) + 2*/); // TODO?
    strcpy(myPath, parent->path);
    // strcat(myPath, "/");
    strcat(myPath, name);
    item->path = myPath;

    if (parent->firstChild == NULL) {
        parent->firstChild = item;
    }
    else {
        HPI_Item_2 *lastChild = parent->firstChild;
        while (lastChild->nextSibling != NULL) {
            lastChild = lastChild->nextSibling;
        }

        lastChild->nextSibling = item;
    }

    parent->dir->fileCount++;

    rebuild_dir_buffer_2(hpi);

    // --> recompress the names and directory buffers

    byte *namesBlock = NULL;    // aka chunk + compressed namesBuff
    byte *dirBlock = NULL;      // aka chunk + compressed directoryBuff
    uint32_t namesBlockLen;
    uint32_t dirBlockLen;

    Chunk namesChunk;
    namesChunk.marker = SQSH_MARKER;
    namesChunk.unknown1 = 0;
    namesChunk.compMethod = 2;
    namesChunk.isEncrypted = 0;
    namesChunk.compressedSize = 0;
    namesChunk.decompressedSize = hpi->namesBuffLen;
    namesChunk.checksum = 0;  // null-init

    testlog("Recompressing hpi->namesBuff\n");

    uint32_t compNamesLen = recompress(hpi->namesBuff, &namesChunk, &namesBlock);
    namesChunk.compressedSize = compNamesLen;
    namesBlockLen = compNamesLen;

    Chunk dirChunk;
    dirChunk.marker = SQSH_MARKER;
    dirChunk.unknown1 = 0;
    dirChunk.compMethod = 2;
    dirChunk.isEncrypted = 0;
    dirChunk.compressedSize = 0;
    dirChunk.decompressedSize = hpi->dirBuffLen;
    dirChunk.checksum = 0;  // null-init

    testlog("Recompressing hpi->directoryBuff\n");

    uint32_t compDirLen = recompress(hpi->directoryBuff, &dirChunk, &dirBlock);
    dirChunk.compressedSize = compDirLen;
    dirBlockLen = compDirLen;

    // TODO here, I could do something like check if the N or D section comes first, etc
    // ... in the future, support to F+D+N (instead of F+N+D) could easily be implemented
    // ... if D < N, nameSection is the one that moves to the right, so when writing to the
    // ...  file, first the names block should be written, then the dir block
    testlog("dirSection: %u -> ", dirSection);
    dirSection = nameSection + sizeof(Chunk) + compNamesLen;
    testlog("%u\n", dirSection);

    // TODO RECOMPRESS DIRECTORY BLOCK AND STORE DIR INCREMENT
    // dirIncrement = ...

    // --> write the buffers to disk
    // write the datas from right to left so it doesnt overwrite existing data
    // write directory data
    // write names data
    // write files data

    // news: writing from right to left is PROBABLY unneeded... lol
    // this whole "add_x" code needs some heavy refactoring

    // <= because the fileSection might not move in case an empty (0 bytes) file is "inserted"
    assert(fileSection <= nameSection);
    assert(nameSection < dirSection);

    FILE *file = hpi->file;

    fseek(file, dirSection, SEEK_SET);
    fwrite(&dirChunk, sizeof(Chunk), 1, file);
    fwrite(dirBlock, dirBlockLen, 1, file);

    fseek(file, nameSection, SEEK_SET);
    fwrite(&namesChunk, sizeof(Chunk), 1, file);
    fwrite(namesBlock, namesBlockLen, 1, file);

    fseek(file, fileSection, SEEK_SET);
    if (didCompress/*outChunk && outBlock*/) {
        fwrite(outChunk, sizeof(Chunk), 1, file);
        fwrite(outBlock, outChunk->compressedSize, 1, file);
    }
    else {
        fwrite(data, dataSize, 1, file);
    }

    free(dirBlock);
    free(namesBlock);

    // --> edit the header and commit it back into the file

    hpi->header.dirBlockPtr = dirSection;
    hpi->header.dirBlockLen = sizeof(Chunk) + dirBlockLen;
    hpi->header.namesBlockPtr = nameSection;
    hpi->header.namesBlockLen = sizeof(Chunk) + namesBlockLen;
    hpi->header.data = hpi->header.data; // TODO find out what is this
    hpi->header.last78 = hpi->header.last78; // TODO find out what is this

    fseek(file, 8, SEEK_SET); // 8 = after the HAPI marker (4bytes) and the version (4bytes)
    fwrite(&hpi->header, sizeof(Header_2), 1, file);
}

void update_file_2(HPI_2 *hpi, char *data, uint32_t dataSize, HPI_Item_2 *item)
{
    // TODO actually update instead of deleting -> adding

    HPI_Item_2 *parent = item->parent;
    char *name = malloc(strlen(item->name) + 1); strcpy(name, item->name);

    delete_file_2(hpi, item);
    add_file_2(hpi, data, dataSize, name, parent);
}

HPI_Item_2 *make_dir_2(HPI_2 *hpi, char *name, HPI_Item_2 *parent)
{
    assert(parent->isDirectory);

    char *pathname = malloc(1 + strlen(name) + 1);
    strcpy(pathname, "/");
    strcat(pathname, name);
    HPI_Item_2 *existing = find_item_2(parent, pathname, true);
    free(pathname);

    if (existing != NULL) {
        testlog("Directory already exists, using it: %s\n", name);
        return existing;
    }

    uint32_t fileSection = last_positional_item_2(hpi->rootItem, 0);
    uint32_t nameSection = hpi->header.namesBlockPtr;
    uint32_t dirSection = hpi->header.dirBlockPtr;
    testlog("F:%d N:%d D:%d\n", fileSection, nameSection, dirSection);

    if (assert_order_2(hpi, 'F', 'N', 'D')) {
        return NULL;
    }

    uint32_t filesIncrement = 0; // < refers to the files data section, not from the whole FILE
    uint32_t namesIncrement = 0;
    uint32_t dirIncrement = 0;

    // --> create a name and add it to nameBuff

    uint32_t newNamePtr = hpi->namesBuffLen;
    int32_t namesPosDelta = add_name_to_buffer_2(hpi, name);
    assert(namesPosDelta >= 0);

    namesIncrement = (unsigned int) namesPosDelta;

    // --> create directory

    Directory_2 *directory = malloc(sizeof(Directory_2));
    directory->namePtr = newNamePtr;
    directory->firstSubDirPtr = 0;
    directory->subDirCount = 0;
    directory->firstFilePtr = 0;
    directory->fileCount = 0;

    // --> create an item and insert into the parent directory

    HPI_Item_2 *item = malloc(sizeof(HPI_Item_2));
    item->name = malloc(sizeof(name)); strcpy(item->name, name);
    item->path = NULL;
    item->size = 0;
    item->isDirectory = 1;
    item->dir = directory;
    item->entry = NULL;
    item->parent = parent;
    item->nextSibling = NULL;
    item->firstChild = NULL;
    item->origin = 0;

    char *myPath = malloc(255); // TODO dynamic
    strcpy(myPath, parent->path);
    strcat(myPath, name);
    strcat(myPath, "/");

    item->path = myPath;

    if (parent->firstChild == NULL) {
        parent->firstChild = item;
    }
    else {
        HPI_Item_2 *lastChild = parent->firstChild;
        while (lastChild->nextSibling != NULL) {
            lastChild = lastChild->nextSibling;
        }

        lastChild->nextSibling = item;
    }

    parent->dir->subDirCount++;

    rebuild_dir_buffer_2(hpi);

    // --> recompress the names and directory buffers

    byte *namesBlock = NULL;
    byte *dirBlock = NULL;
    uint32_t namesBlockLen;
    uint32_t dirBlockLen;

    Chunk namesChunk;
    namesChunk.marker = SQSH_MARKER;
    namesChunk.unknown1 = 0;
    namesChunk.compMethod = 2;
    namesChunk.isEncrypted = 0;
    namesChunk.compressedSize = 0;
    namesChunk.decompressedSize = hpi->namesBuffLen;
    namesChunk.checksum = 0;

    testlog("Recompressing hpi->namesBuff\n");

    uint32_t compNamesLen = recompress(hpi->namesBuff, &namesChunk, &namesBlock);
    namesChunk.compressedSize = compNamesLen;
    namesBlockLen = compNamesLen;

    Chunk dirChunk;
    dirChunk.marker = SQSH_MARKER;
    dirChunk.unknown1 = 0;
    dirChunk.compMethod = 2;
    dirChunk.isEncrypted = 0;
    dirChunk.compressedSize = 0;
    dirChunk.decompressedSize = hpi->dirBuffLen;
    dirChunk.checksum = 0;

    testlog("Recompressint hpi->directoryBuff\n");

    uint32_t compDirLen = recompress(hpi->directoryBuff, &dirChunk, &dirBlock);
    dirChunk.compressedSize = compDirLen;
    dirBlockLen = compDirLen;

    testlog("dirSection: %u -> ", dirSection);
    dirSection = nameSection + sizeof(Chunk) + compNamesLen;
    testlog("%u\n", dirSection);

    // --> write the buffers to disk

    assert(fileSection <= nameSection);
    assert(nameSection < dirSection);

    FILE *file = hpi->file;

    fseek(file, dirSection, SEEK_SET);
    fwrite(&dirChunk, sizeof(Chunk), 1, file);
    fwrite(dirBlock, dirBlockLen, 1, file);

    fseek(file, nameSection, SEEK_SET);
    fwrite(&namesChunk, sizeof(Chunk), 1, file);
    fwrite(namesBlock, namesBlockLen, 1, file);

    free(dirBlock);
    free(namesBlock);

    // --> edit the header and commit it back into the file

    hpi->header.dirBlockPtr = dirSection;
    hpi->header.dirBlockLen = sizeof(Chunk) + dirBlockLen;
    hpi->header.namesBlockPtr = nameSection;
    hpi->header.namesBlockLen = sizeof(Chunk) + namesBlockLen;
    hpi->header.data = hpi->header.data; // TODO find out what is this
    hpi->header.last78 = hpi->header.last78; // TODO find out what is this

    fseek(file, 8, SEEK_SET); // 8 = after the HAPI marker (4bytes) and the version (4bytes)
    fwrite(&hpi->header, sizeof(Header_2), 1, file);

    return item;
}

bool delete_file_2(HPI_2 *hpi, HPI_Item_2 *fileItem)
{
    testlog("Deleting (F) %s\n", fileItem->path);

    uint32_t fileSectionEnd = last_positional_item_2(hpi->rootItem, 0);
    uint32_t nameSection = hpi->header.namesBlockPtr;
    uint32_t dirSection = hpi->header.dirBlockPtr;
    testlog("F:%d N:%d D:%d\n", fileSectionEnd, nameSection, dirSection);

    if (assert_order_2(hpi, 'F', 'N', 'D')) {
        return false;
    }

    // --> delete the name from the nameBuff
    // --> then traverse all items in the hpi to update their namePtr

    // TODO check beforehand if this namePtr isn't reused by any other entry?
    int32_t namesPosDelta = 0;//delete_name_from_buffer(hpi, fileItem->entry->namePtr);
    assert(namesPosDelta <= 0);

    if (namesPosDelta != 0) {
        update_name_pointers(hpi->rootItem, fileItem->entry->namePtr, namesPosDelta);
    }

    // --> assign file decrement, move sections and update file pointers

    uint32_t dataPos = fileItem->entry->dataStartPtr;
    uint32_t dataLen = fileItem->entry->compressedSize ?
        fileItem->entry->compressedSize : fileItem->entry->decompressedSize;

    fileSectionEnd -= dataLen;
    nameSection -= dataLen;
    dirSection -= dataLen;

    int32_t dataPosDelta = -1 * ((int32_t) dataLen);

    update_file_pointers(hpi->rootItem, dataPos, dataPosDelta);

    // --> remove the item from the parent directory and rebuild buffer

    HPI_Item_2 *parent = fileItem->parent;

    if (parent->firstChild == fileItem) {
        parent->firstChild = fileItem->nextSibling;
    }
    else {
        HPI_Item_2 *prevChild = parent->firstChild;
        while (prevChild != NULL) {
            if (prevChild->nextSibling == fileItem) {
                prevChild->nextSibling = fileItem->nextSibling;
                break;
            }

            prevChild = prevChild->nextSibling;
        }
    }

    free(fileItem->name);
    free(fileItem->path);
    free(fileItem);

    testlog("Decrementing parent's file count: %u -> %u\n",
        parent->dir->fileCount, parent->dir->fileCount - 1);
    parent->dir->fileCount--;

    rebuild_dir_buffer_2(hpi);

    // --> recompress the names and directory buffers

    byte *namesBlock = NULL;    // aka chunk + compressed namesBuff
    byte *dirBlock = NULL;      // aka chunk + compressed directoryBuff
    uint32_t namesBlockLen;
    uint32_t dirBlockLen;

    Chunk namesChunk;
    namesChunk.marker = SQSH_MARKER;
    namesChunk.unknown1 = 0;
    namesChunk.compMethod = 2;
    namesChunk.isEncrypted = 0;
    namesChunk.compressedSize = 0;
    namesChunk.decompressedSize = hpi->namesBuffLen;
    namesChunk.checksum = 0;  // null-init

    testlog("Recompressing hpi->namesBuff\n");

    uint32_t compNamesLen = recompress(hpi->namesBuff, &namesChunk, &namesBlock);
    namesChunk.compressedSize = compNamesLen;
    namesBlockLen = compNamesLen;

    Chunk dirChunk;
    dirChunk.marker = SQSH_MARKER;
    dirChunk.unknown1 = 0;
    dirChunk.compMethod = 2;
    dirChunk.isEncrypted = 0;
    dirChunk.compressedSize = 0;
    dirChunk.decompressedSize = hpi->dirBuffLen;
    dirChunk.checksum = 0;  // null-init

    testlog("Recompressing hpi->directoryBuff\n");

    uint32_t compDirLen = recompress(hpi->directoryBuff, &dirChunk, &dirBlock);
    dirChunk.compressedSize = compDirLen;
    dirBlockLen = compDirLen;

    testlog("dirSection: %u -> ", dirSection);
    dirSection = nameSection + sizeof(Chunk) + compNamesLen;
    testlog("%u\n", dirSection);

    // --> write the buffers to disk

    testlog("F:%d N:%d D:%d\n", fileSectionEnd, nameSection, dirSection);

    assert(fileSectionEnd <= nameSection);
    assert(nameSection < dirSection);

    FILE *file = hpi->file;

    uint32_t dataAfterLen = fileSectionEnd - dataPos;
    testlog("dataPos: %u\n", dataPos);
    testlog("dataLen: %u\n", dataLen);
    testlog("dataAfterLen: %u\n", dataAfterLen);

    if (dataLen && dataAfterLen > 0) {
        byte *dataAfterBuf = malloc(dataAfterLen);

        fseek(file, dataPos + dataLen, SEEK_SET);
        fread(dataAfterBuf, dataAfterLen, 1, file);

        fseek(file, dataPos, SEEK_SET);
        fwrite(dataAfterBuf, dataAfterLen, 1, file);

        free(dataAfterBuf);
    }

    fseek(file, nameSection, SEEK_SET);
    fwrite(&namesChunk, sizeof(Chunk), 1, file);
    fwrite(namesBlock, namesBlockLen, 1, file);

    fseek(file, dirSection, SEEK_SET);
    fwrite(&dirChunk, sizeof(Chunk), 1, file);
    fwrite(dirBlock, dirBlockLen, 1, file);

    free(dirBlock);
    free(namesBlock);

    uint32_t fileEnd = dirSection + sizeof(Chunk) + dirBlockLen;
    testlog("New file end: %u\n", fileEnd);

    if (ftruncate(fileno(file), fileEnd) != 0) {
        // TODO failed to truncate file size
    }

    // --> edit the header and commit it back into the file

    hpi->header.dirBlockPtr = dirSection;
    hpi->header.dirBlockLen = sizeof(Chunk) + dirBlockLen;
    hpi->header.namesBlockPtr = nameSection;
    hpi->header.namesBlockLen = sizeof(Chunk) + namesBlockLen;
    hpi->header.data = hpi->header.data; // TODO find out what is this
    hpi->header.last78 = hpi->header.last78; // TODO find out what is this

    fseek(file, 8, SEEK_SET); // 8 = after the HAPI marker (4bytes) and the version (4bytes)
    fwrite(&hpi->header, sizeof(Header_2), 1, file);

    puts("-------------------------");
    return true;
}

bool delete_dir_2(HPI_2 *hpi, HPI_Item_2 *dirItem)
{
    testlog("Deleting (D) %s (%d)\n", dirItem->path, dirItem->dir->subDirCount + dirItem->dir->fileCount);

    uint32_t fileSectionEnd = last_positional_item_2(hpi->rootItem, 0);
    uint32_t nameSection = hpi->header.namesBlockPtr;
    uint32_t dirSection = hpi->header.dirBlockPtr;
    testlog("F:%d N:%d D:%d\n", fileSectionEnd, nameSection, dirSection);

    if (assert_order_2(hpi, 'F', 'N', 'D')) {
        return false;
    }

    // --> delete the name from the nameBuff
    // --> then traverse all items in the hpi to update their namePtr

    // TODO check beforehand if this namePtr isn't reused by any other entry?
    int32_t namesPosDelta = delete_name_from_buffer(hpi, dirItem->dir->namePtr);
    assert(namesPosDelta <= 0);

    if (namesPosDelta != 0) {
        update_name_pointers(hpi->rootItem, dirItem->dir->namePtr, namesPosDelta);
    }

    // --> remove the item from the parent directory and rebuild buffer

    HPI_Item_2 *parent = dirItem->parent;

    if (parent->firstChild == dirItem) {
        parent->firstChild = dirItem->nextSibling;
    }
    else {
        HPI_Item_2 *prevChild = parent->firstChild;
        while (prevChild != NULL) {
            if (prevChild->nextSibling == dirItem) {
                prevChild->nextSibling = dirItem->nextSibling;
                break;
            }

            prevChild = prevChild->nextSibling;
        }
    }

    free(dirItem->name);
    free(dirItem->path);
    free(dirItem);

    testlog("Decrementing parent's file count: %u -> %u\n",
        parent->dir->subDirCount, parent->dir->subDirCount - 1);
    parent->dir->subDirCount--;

    rebuild_dir_buffer_2(hpi);

    // --> recompress the names and directory buffers

    byte *namesBlock = NULL;    // aka chunk + compressed namesBuff
    byte *dirBlock = NULL;      // aka chunk + compressed directoryBuff
    uint32_t namesBlockLen;
    uint32_t dirBlockLen;

    Chunk namesChunk;
    namesChunk.marker = SQSH_MARKER;
    namesChunk.unknown1 = 0;
    namesChunk.compMethod = 2;
    namesChunk.isEncrypted = 0;
    namesChunk.compressedSize = 0;
    namesChunk.decompressedSize = hpi->namesBuffLen;
    namesChunk.checksum = 0;  // null-init

    testlog("Recompressing hpi->namesBuff\n");

    uint32_t compNamesLen = recompress(hpi->namesBuff, &namesChunk, &namesBlock);
    namesChunk.compressedSize = compNamesLen;
    namesBlockLen = compNamesLen;

    Chunk dirChunk;
    dirChunk.marker = SQSH_MARKER;
    dirChunk.unknown1 = 0;
    dirChunk.compMethod = 2;
    dirChunk.isEncrypted = 0;
    dirChunk.compressedSize = 0;
    dirChunk.decompressedSize = hpi->dirBuffLen;
    dirChunk.checksum = 0;  // null-init

    testlog("Recompressing hpi->directoryBuff\n");

    uint32_t compDirLen = recompress(hpi->directoryBuff, &dirChunk, &dirBlock);
    dirChunk.compressedSize = compDirLen;
    dirBlockLen = compDirLen;

    testlog("dirSection: %u -> ", dirSection);
    //dirSection += sizeof(Chunk) + compNamesLen;
    dirSection = nameSection + sizeof(Chunk) + compNamesLen;
    testlog("%u\n", dirSection);

    // --> write the buffers to disk

    testlog("F:%d N:%d D:%d\n", fileSectionEnd, nameSection, dirSection);

    assert(fileSectionEnd <= nameSection);
    assert(nameSection < dirSection);

    FILE *file = hpi->file;

    fseek(file, nameSection, SEEK_SET);
    fwrite(&namesChunk, sizeof(Chunk), 1, file);
    fwrite(namesBlock, namesBlockLen, 1, file);

    fseek(file, dirSection, SEEK_SET);
    fwrite(&dirChunk, sizeof(Chunk), 1, file);
    fwrite(dirBlock, dirBlockLen, 1, file);

    free(dirBlock);
    free(namesBlock);

    uint32_t fileEnd = dirSection + sizeof(Chunk) + dirBlockLen;
    testlog("New file end: %u\n", fileEnd);

    if (ftruncate(fileno(file), fileEnd) != 0) {
        // TODO failed to truncate file size
    }

    // --> edit the header and commit it back into the file

    hpi->header.dirBlockPtr = dirSection;
    hpi->header.dirBlockLen = sizeof(Chunk) + dirBlockLen;
    hpi->header.namesBlockPtr = nameSection;
    hpi->header.namesBlockLen = sizeof(Chunk) + namesBlockLen;
    hpi->header.data = hpi->header.data; // TODO find out what is this
    hpi->header.last78 = hpi->header.last78; // TODO find out what is this

    fseek(file, 8, SEEK_SET); // 8 = after the HAPI marker (4bytes) and the version (4bytes)
    fwrite(&hpi->header, sizeof(Header_2), 1, file);

    puts("-------------------------");

    return true;
}

bool delete_auto_2(HPI_2 *hpi, HPI_Item_2 *item)
{
    if (item->isDirectory) {
        HPI_Item_2 *nextItem = item->firstChild;
        while (nextItem != NULL) {
            HPI_Item_2 *curItem = nextItem; // because nextItem gets free'd
            nextItem = nextItem->nextSibling;

            if (!delete_auto_2(hpi, curItem)) {
                // TODO error
                return false;
            }
        }

        return delete_dir_2(hpi, item);
    }
    else {
        return delete_file_2(hpi, item);
    };
}

void rename_2(HPI_2 *hpi, HPI_Item_2 *item, char *name)
{
    uint32_t fileSectionEnd = last_positional_item_2(hpi->rootItem, 0);
    uint32_t nameSection = hpi->header.namesBlockPtr;
    uint32_t dirSection = hpi->header.dirBlockPtr;
    testlog("F:%d N:%d D:%d\n", fileSectionEnd, nameSection, dirSection);

    if (assert_order_2(hpi, 'F', 'N', 'D')) {
        return;
    }

    int32_t namesIncrement = 0;

    // --> delete the name from the nameBuff
    // --> then traverse all items in the hpi to update their namePtr
    // --> then create a name and add it to nameBuff

    uint32_t *namePtr = item->isDirectory ? &item->dir->namePtr : &item->entry->namePtr;
    // TODO check beforehand if this namePtr isn't reused by any other entry?
    int32_t namesPosDelta = delete_name_from_buffer(hpi, *namePtr);
    assert(namesPosDelta <= 0);

    if (namesPosDelta != 0) {
        update_name_pointers(hpi->rootItem, *namePtr, namesPosDelta);
    }

    *namePtr = hpi->namesBuffLen; // updates for either item->dir or item->entry
    namesPosDelta += add_name_to_buffer_2(hpi, name);

    puts("Printing newBuffer again:");
    fwrite(hpi->namesBuff, hpi->namesBuffLen, 1, stdout); puts("");

    namesIncrement += namesPosDelta;

    testlog("namesIncrement: %d\n", namesIncrement);

    // --> update the item's name and path, then rebuild the dir buffer

    char *parentPath = item->parent->path;

    free(item->name);
    free(item->path);
    // item->name = name; // item->name needs to be dynamic to call free
    item->name = malloc(strlen(name) + 1); strcpy(item->name, name);
    item->path = malloc(strlen(parentPath) + strlen(name) + 1);
    strcpy(item->path, parentPath);
    strcat(item->path, name);

    testlog("Path: %s\n", item->path);

    rebuild_dir_buffer_2(hpi);

    // --> recompress the names and directory buffers

    byte *namesBlock = NULL;    // aka chunk + compressed namesBuff
    byte *dirBlock = NULL;      // aka chunk + compressed directoryBuff
    uint32_t namesBlockLen;
    uint32_t dirBlockLen;

    Chunk namesChunk;
    namesChunk.marker = SQSH_MARKER;
    namesChunk.unknown1 = 0;
    namesChunk.compMethod = 2;
    namesChunk.isEncrypted = 0;
    namesChunk.compressedSize = 0;
    namesChunk.decompressedSize = hpi->namesBuffLen;
    namesChunk.checksum = 0;  // null-init

    testlog("Recompressing hpi->namesBuff\n");

    uint32_t compNamesLen = recompress(hpi->namesBuff, &namesChunk, &namesBlock);
    namesChunk.compressedSize = compNamesLen;
    namesBlockLen = compNamesLen;

    Chunk dirChunk;
    dirChunk.marker = SQSH_MARKER;
    dirChunk.unknown1 = 0;
    dirChunk.compMethod = 2;
    dirChunk.isEncrypted = 0;
    dirChunk.compressedSize = 0;
    dirChunk.decompressedSize = hpi->dirBuffLen;
    dirChunk.checksum = 0;  // null-init

    testlog("Recompressing hpi->directoryBuff\n");

    uint32_t compDirLen = recompress(hpi->directoryBuff, &dirChunk, &dirBlock);
    dirChunk.compressedSize = compDirLen;
    dirBlockLen = compDirLen;

    // dirSection += sizeof(Chunk) + compNamesLen;
    testlog("dirSection: %u -> ", dirSection);
    dirSection = nameSection + sizeof(Chunk) + compNamesLen;
    testlog("%u\n", dirSection);

    // --> write the buffers to disk

    testlog("F:%d N:%d D:%d\n", fileSectionEnd, nameSection, dirSection);

    assert(fileSectionEnd <= nameSection);
    assert(nameSection < dirSection);

    FILE *file = hpi->file;

    fseek(file, nameSection, SEEK_SET);
    fwrite(&namesChunk, sizeof(Chunk), 1, file);
    fwrite(namesBlock, namesBlockLen, 1, file);

    fseek(file, dirSection, SEEK_SET);
    fwrite(&dirChunk, sizeof(Chunk), 1, file);
    fwrite(dirBlock, dirBlockLen, 1, file);

    free(dirBlock);
    free(namesBlock);

    uint32_t fileEnd = dirSection + sizeof(Chunk) + dirBlockLen;
    uint32_t oldEnd = hpi->header.dirBlockPtr + hpi->header.dirBlockLen;

    testlog("New end: %u; Old end: %u\n", fileEnd, oldEnd);

    if (fileEnd < oldEnd) {
        if (ftruncate(fileno(file), fileEnd) != 0) {
            // TODO failed to truncate file size
        }
    }

    // --> edit the header and commit it back into the file

    hpi->header.dirBlockPtr = dirSection;
    hpi->header.dirBlockLen = sizeof(Chunk) + dirBlockLen;
    hpi->header.namesBlockPtr = nameSection;
    hpi->header.namesBlockLen = sizeof(Chunk) + namesBlockLen;
    hpi->header.data = hpi->header.data; // TODO find out what is this
    hpi->header.last78 = hpi->header.last78; // TODO find out what is this

    fseek(file, 8, SEEK_SET); // 8 = after the HAPI marker (4bytes) and the version (4bytes)
    fwrite(&hpi->header, sizeof(Header_2), 1, file);

    return;
}

void move_item_2(HPI_2 *hpi, HPI_Item_2 *item, HPI_Item_2 *newParent)
{
    if (!newParent->isDirectory) {
        puts("Parent isn't directory, aborting...");
        return;
    }

    if (item->parent == newParent) {
        puts("Move operation is redundant. Skipping it.");
        return;
    }

    uint32_t fileSectionEnd = last_positional_item_2(hpi->rootItem, 0);
    uint32_t nameSection = hpi->header.namesBlockPtr;
    uint32_t dirSection = hpi->header.dirBlockPtr;
    testlog("F:%d N:%d D:%d\n", fileSectionEnd, nameSection, dirSection);

    if (assert_order_2(hpi, 'F', 'N', 'D')) {
        return;
    }

    // --> update the item's path

    free(item->path);
    item->path = malloc(strlen(newParent->path) + strlen(item->name) + 1);
    strcpy(item->path, newParent->path);
    strcat(item->path, item->name);

    // --> update the item's directory structure, then rebuild the buffer

    HPI_Item_2 *parent = item->parent;
    if (parent->firstChild == item) {
        parent->firstChild = item->nextSibling;
        testlog("%s->firstChild is now %s\n", parent->name,
            parent->firstChild->name);
    }
    else {
        HPI_Item_2 *prevChild = parent->firstChild;
        while (prevChild != NULL) {
            if (prevChild->nextSibling == item) {
                prevChild->nextSibling = item->nextSibling;
                testlog("%s->nextSibling is now %s\n", prevChild->name,
                    prevChild->nextSibling->name);
                break;
            }

            prevChild = prevChild->nextSibling;
        }
    }

    if (newParent->firstChild == NULL) {
        newParent->firstChild = item;
        testlog("%s->firstChild is now %s\n", newParent->name,
            newParent->firstChild->name);
    }
    else {
        HPI_Item_2 *lastChild = newParent->firstChild;
        while (lastChild->nextSibling != NULL) {
            lastChild = lastChild->nextSibling;
        }

        lastChild->nextSibling = item;
        testlog("%s->nextSibling is now %s\n", lastChild->name,
            lastChild->nextSibling->name);
    }

    item->nextSibling = NULL;
    item->parent = newParent;

    if (item->isDirectory) {
        parent->dir->subDirCount--;
        newParent->dir->subDirCount++;
    }
    else {
        testlog("Decrementing parent's file count: %u -> %u\n",
            parent->dir->fileCount, parent->dir->fileCount - 1);
        testlog("Incrementing newParent's file count: %u -> %u\n",
            newParent->dir->fileCount, newParent->dir->fileCount + 1);

        parent->dir->fileCount--;
        newParent->dir->fileCount++;
    }

    rebuild_dir_buffer_2(hpi);

    // --> recompress the names and directory buffers

    byte *dirBlock = NULL;      // aka chunk + compressed directoryBuff
    uint32_t dirBlockLen;

    Chunk dirChunk;
    dirChunk.marker = SQSH_MARKER;
    dirChunk.unknown1 = 0;
    dirChunk.compMethod = 2;
    dirChunk.isEncrypted = 0;
    dirChunk.compressedSize = 0;
    dirChunk.decompressedSize = hpi->dirBuffLen;
    dirChunk.checksum = 0;  // null-init

    testlog("Recompressing hpi->directoryBuff\n");

    uint32_t compDirLen = recompress(hpi->directoryBuff, &dirChunk, &dirBlock);
    dirChunk.compressedSize = compDirLen;
    dirBlockLen = compDirLen;

    // no need to move dirSection's location since F and N dont change
    // but assign anyway because sometimes there's garbage between
    // nameSectionEnd and dirSection
    dirSection = hpi->header.namesBlockPtr + hpi->header.namesBlockLen;

    // --> write the buffers to disk

    testlog("F:%d N:%d (%u) D:%d (%u)\n", fileSectionEnd, nameSection,
        hpi->header.namesBlockLen, dirSection, hpi->header.dirBlockLen);

    assert(fileSectionEnd <= nameSection);
    assert(nameSection < dirSection);

    FILE *file = hpi->file;

    fseek(file, dirSection, SEEK_SET);
    fwrite(&dirChunk, sizeof(Chunk), 1, file);
    fwrite(dirBlock, dirBlockLen, 1, file);

    free(dirBlock);

    uint32_t fileEnd = dirSection + sizeof(Chunk) + dirBlockLen;
    uint32_t oldEnd = hpi->header.dirBlockPtr + hpi->header.dirBlockLen;

    testlog("New end: %u; Old end: %u\n", fileEnd, oldEnd);

    if (fileEnd < oldEnd) {
        if (ftruncate(fileno(file), fileEnd) != 0) {
            // TODO failed to truncate file size
        }
    }

    // --> edit the header and commit it back into the file

    hpi->header.dirBlockPtr = dirSection;
    hpi->header.dirBlockLen = sizeof(Chunk) + dirBlockLen;
    hpi->header.data = hpi->header.data; // TODO find out what is this
    hpi->header.last78 = hpi->header.last78; // TODO find out what is this

    fseek(file, 8, SEEK_SET); // 8 = after the HAPI marker (4bytes) and the version (4bytes)
    fwrite(&hpi->header, sizeof(Header_2), 1, file);
}

uint32_t compress_file_2(char *data, uint32_t dataSize, Entry_2 *ioEntry,
                        byte **outBlockPtr, Chunk **outChunkPtr)
{
    uint8_t compMethod = 2; // ZLIB

    byte *outBlock = NULL;
    Chunk *outChunk = NULL;

    check_calc_2(&ioEntry->checksum, (byte *) data, dataSize);

    if (dataSize <= sizeof(Chunk)) {
        compMethod = 0; // no point bothering compressing
    }

    if (compMethod) {
        outChunk = malloc(sizeof(Chunk));
        outChunk->marker = SQSH_MARKER;
        outChunk->unknown1 = 0x02;
        outChunk->compMethod = compMethod;
        outChunk->isEncrypted = 0; // no reason to encrypt I guess
        outChunk->compressedSize = 0;
        outChunk->decompressedSize = dataSize;
        outChunk->checksum = 0; // null init

        uint32_t compSize = compressZLib(data, dataSize, &outBlock);

        assert(compSize > 0);

        if ((sizeof(Chunk) + compSize) > outChunk->decompressedSize) {
            // if the compressed result is somohow larger than the original decompressed data
            // ... then there is no point compressing at all
            compSize = 0;
            compMethod = 0;
            free(outChunk);
            free(outBlock);
            return 0;
        }

        byte n;
        for (uint32_t x = 0; x < compSize; x++) {
            n = (unsigned char) outBlock[x];
            if (outChunk->isEncrypted)	{
                n = (n ^ x) + x;
                outBlock[x] = n;
            }
            outChunk->checksum += n;
        }

        outChunk->compressedSize = compSize;

        *outBlockPtr = outBlock;
        *outChunkPtr = outChunk;
        // return sizeof(Chunk) + outChunk->compressedSize;
        return 1;
        //fwrite(outChunk, sizeof(*outChunk), 1, HPIFile); // TODO sizeof(*outChunk) == sizeof(Chunk)????
        //fwrite(outBlock, outChunk->CompressedSize, 1, HPIFile);
    }

    /*FileCount += d->count;
    UncompTotal += d->count;*/

    //ioEntry->compressedSize = 0;
    //fwrite(InBlock, d->count, 1, HPIFile);
    //outBlock = inBlock;
    return 0;
}
