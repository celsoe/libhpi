#ifndef LIBHPI_IOUTILS_H
#define LIBHPI_IOUTILS_H

#include <stdio.h>

int insert_buffer_at(FILE *file, uint32_t fileOffset, char *data, uint32_t dataLen);

#endif
