# LibHPI

This is a library (and CLI program) that can be used to manipulate HPI files in several ways. This was initially created to study and better understand the .hpi file format. The program has grown into quite a mess so another version made from scratch (but using this codebase as a foundation) is planned.

Operations currently supported:

- [ ] `Create` **(c)** Create an empty HPI from nothing
- [X] `Add` **(a)** Add files/directories to the HPI
- [X] `Extract` **(x)** Extract files/directories from the HPI
- [X] `Mkdir` **(n)** Create an empty directory in the HPI
- [X] `Delete` **(d)** Delete files/directories in the HPI
- [/] `Update` **(u)** Replace the data of an existing file in the HPI
- [X] `Move` **(m)** Move a file/directory into a different directory in the HPI
- [X] `List` **(s)** Print the contents of the HPI
- [X] `Info` **(i)** Print some general info about the HPI

## Example commands:

### Add
Note that you cannot add files that would overwrite existing files in the target HPI. Delete them before replacing them or use the update command.

Add the local files **tarmage.fbi** and **myfile.fbi** into the HPI's directory **unitscb**  
`> libhpi a test.hpi -d /unitscb tarmage.fbi path/to/myfile.fbi`

Add the whole local directory **canbuildcb** into the root of the HPI  
`> libhpi a test.hpi canbuildcb`

### Extract

Extract everything into the current directory  
`> libhpi x test.hpi`

Extract the files **zonhunt.fbi** and **zonfly.fbi** inside the HPI into the local folder **My Documents**  
`> libhpi x test.hpi -o "My Documents" /unitscb/zonhunt.fbi /unitscb/zonfly.fbi`

### Mkdir

zzz TODO: finish the README file later
