#include "libhpi.h"
#include "decompress.h"
#include "compress.h"
#include "logger.h"

#include <stdlib.h>
#include <string.h>

void debug_print_hpi(HPI_2 *hpi)
{
    testlog("\n--- Header -----");

    testlog("\ndirBlockPtr     %u", hpi->header.dirBlockPtr);
    testlog("\ndirBlockLen     %u", hpi->header.dirBlockLen);
    testlog("\nnamesBlockPtr   %u", hpi->header.namesBlockPtr);
    testlog("\nnamesBlockLen   %u", hpi->header.namesBlockLen);

    testlog("\n----------------\n");

    print_tree_2(hpi->rootItem);
}

void debug_print_item(HPI_Item_2 *item)
{
    if (item->isDirectory) {
        Directory_2 *dir = item->dir;

        testlog("\n--- Item [D] ---\n%s", item->path);
        testlog("\n(addr)          0x%02x (%u)", item->origin, item->origin);
        testlog("\nnamePtr         0x%02x (%u)", dir->namePtr, dir->namePtr);
        testlog("\nfirstSubDirPtr  0x%02x (%u)", dir->firstSubDirPtr, dir->firstSubDirPtr);
        testlog("\nsubDirCount     0x%02x (%u)", dir->subDirCount, dir->subDirCount);
        testlog("\nfirstFilePtr    0x%02x (%u)", dir->firstFilePtr, dir->firstFilePtr);
        testlog("\nfileCount       0x%02x (%u)", dir->fileCount, dir->fileCount);
    } else {
        Entry_2 *entry = item->entry;

        testlog("\n--- Item [E] ---\n%s", item->path);
        testlog("\n(addr)          0x%02x (%u)", item->origin, item->origin);
        testlog("\nnamePtr           0x%02x (%u)", entry->namePtr, entry->namePtr);
        testlog("\ndataStartPtr      0x%02x (%u)", entry->dataStartPtr, entry->dataStartPtr);
        testlog("\ndecompressedSize  0x%02x (%u)", entry->decompressedSize, entry->decompressedSize);
        testlog("\ncompressedSize    0x%02x (%u)", entry->compressedSize, entry->compressedSize);
    }

    testlog("\n----------------\n");
}

void load_version(FILE *inFile, Version *outVersion)
{
    fseek(inFile, 0, SEEK_SET);
    fread(outVersion, sizeof(Version), 1, inFile);
}

uint32_t load_buffer(FILE *inFile, byte **outBufferPtr, uint32_t blockPtr, uint32_t blockLen)
{
    byte *block;
    Chunk *chunk;
    uint32_t outSize;

    block = malloc(blockLen);
    fseek(inFile, blockPtr, SEEK_SET); // might not be needed ! dirBlockPtr seems to start right after header
    fread(block, blockLen, 1, inFile);
    chunk = (void *) block;

    if (chunk->marker == SQSH_MARKER) {
        *outBufferPtr = malloc(chunk->decompressedSize);
        outSize = decompress(block + sizeof(Chunk), chunk, *outBufferPtr);

        if (outSize != chunk->decompressedSize) {
            // TODO - return an error!
            testlog("Decmp Error! Expected: %u Got: %u\n", chunk->decompressedSize, outSize);

            free(block);
            return 0;
        }

        free(block);
        return outSize;
    }
    else {
        *outBufferPtr = block;
        return blockLen;
    }
}

void load_2(FILE *inFile, HPI_2 *outHpi)
{
    Header_2 *header = &outHpi->header; // alias
    Directory_2 *directory;

    outHpi->file = inFile;

    load_header_2(inFile, header);
    outHpi->dirBuffLen = load_buffer(inFile, &outHpi->directoryBuff, header->dirBlockPtr, header->dirBlockLen);
    outHpi->namesBuffLen = load_buffer(inFile, &outHpi->namesBuff, header->namesBlockPtr, header->namesBlockLen);

    directory = (Directory_2 *) outHpi->directoryBuff;

    HPI_Item_2 *rootItem = malloc(sizeof(HPI_Item_2));
    rootItem->isDirectory = true;
    rootItem->dir = directory;
    rootItem->size = 0;
    rootItem->name = malloc(2); strcpy(rootItem->name, "");
    rootItem->path = malloc(2); strcpy(rootItem->path, "/");
    rootItem->parent = NULL;
    rootItem->nextSibling = NULL;
    rootItem->firstChild = NULL;
    rootItem->origin = 0;

    outHpi->rootItem = rootItem;

    process_directory_2(outHpi, rootItem, "/");
}

void clean_2(HPI_2 *hpi)
{
    free(hpi->directoryBuff);
    free(hpi->namesBuff);
    free_tree_2(hpi->rootItem);
    //free(hpi);
}

void load_header_2(FILE *inFile, Header_2 *inHeader)
{
    fseek(inFile, sizeof(Version), SEEK_SET);
    fread(inHeader, sizeof(Header_2), 1, inFile);
}

void process_directory_2(HPI_2 *inHpi, HPI_Item_2 *currentItem, char *currentPath)
{
    Directory_2 *inDir = currentItem->dir;
    HPI_Item_2 *lastItem = NULL;

    if (inDir->fileCount != 0) {
        for (int count = 0; count < inDir->fileCount; count++) {
            Entry_2 *nextEntry = (Entry_2 *) (inHpi->directoryBuff + inDir->firstFilePtr) + count;

            char *name = malloc(strlen(inHpi->namesBuff + nextEntry->namePtr) + 1);
            char *myPath = malloc(255/*strlen(name) + strlen(currentPath) + 2*/);

            strcpy(name, inHpi->namesBuff + nextEntry->namePtr);

            strcpy(myPath, currentPath);
            // strcat(myPath, "/");
            strcat(myPath, name);

            // testlog("Created %s\n", myPath);

            HPI_Item_2 *nextItem = malloc(sizeof(HPI_Item_2));
            nextItem->isDirectory = false;
            nextItem->entry = nextEntry;
            nextItem->size = nextEntry->decompressedSize;
            nextItem->name = name;
            nextItem->path = myPath;
            nextItem->parent = currentItem;
            nextItem->nextSibling = NULL;
            nextItem->firstChild = NULL;
            // nextItem->origin = (void *) nextEntry - (void *) inHpi->directoryBuff;
            // /\ same as above \/
            nextItem->origin = (void *) (
                inDir->firstFilePtr + (count * sizeof(Entry_2))
            );

            if (currentItem->firstChild == NULL) {
                currentItem->firstChild = nextItem;
            }

            if (lastItem != NULL) {
                lastItem->nextSibling = nextItem;
            }

            lastItem = nextItem;
        }
    }

    if (inDir->subDirCount != 0) {
        for (int count = 0; count < inDir->subDirCount; count++) {
            Directory_2 *nextSubDir = (Directory_2 *) (inHpi->directoryBuff + inDir->firstSubDirPtr) + count;

            char *name = malloc(strlen(inHpi->namesBuff + nextSubDir->namePtr) + 1);
            char *myPath = malloc(255/*sizeof(name) + sizeof(currentPath) + 2*/); // TODO?

            strcpy(name, inHpi->namesBuff + nextSubDir->namePtr);

            strcpy(myPath, currentPath);
            strcat(myPath, name);
            strcat(myPath, "/");

            //testlog("%s\n", myPath);

            HPI_Item_2 *nextItem = malloc(sizeof(HPI_Item_2));
            nextItem->isDirectory = true;
            nextItem->dir = nextSubDir;
            nextItem->size = 0;
            nextItem->name = name;
            nextItem->path = myPath;
            nextItem->parent = currentItem;
            nextItem->nextSibling = NULL;
            nextItem->firstChild = NULL;
            // nextItem->origin = (void *) nextSubDir - (void *) inHpi->directoryBuff;
            nextItem->origin = (void *) (
                inDir->firstSubDirPtr + (count * sizeof(Directory_2))
            );

            if (currentItem->firstChild == NULL) {
                currentItem->firstChild = nextItem;
            }

            if (lastItem != NULL) {
                lastItem->nextSibling = nextItem;
            }

            lastItem = nextItem;

            process_directory_2(inHpi, nextItem, myPath);
            // process_entries_2(inHpi, nextItem, myPath);
        }
    }
}

void process_entries_2(HPI_2 *inHpi, HPI_Item_2 *currentItem, char *currentPath)
{
    Directory_2 *inDir = currentItem->dir;
    HPI_Item_2 *lastItem = NULL;

    if (inDir->fileCount != 0) {
        for (int count = 0; count < inDir->fileCount; count++) {
            Entry_2 *nextEntry = (Entry_2 *) (inHpi->directoryBuff + inDir->firstFilePtr) + count;

            char *name = malloc(strlen(inHpi->namesBuff + nextEntry->namePtr) + 1);
            char *myPath = malloc(255/*strlen(name) + strlen(currentPath) + 2*/);

            strcpy(name, inHpi->namesBuff + nextEntry->namePtr);

            strcpy(myPath, currentPath);
            // strcat(myPath, "/");
            strcat(myPath, name);

            //testlog("%s\n", myPath);

            HPI_Item_2 *nextItem = malloc(sizeof(HPI_Item_2));
            nextItem->isDirectory = false;
            nextItem->entry = nextEntry;
            nextItem->size = nextEntry->decompressedSize;
            nextItem->name = name;
            nextItem->path = myPath;
            nextItem->parent = currentItem;
            nextItem->nextSibling = NULL;
            nextItem->firstChild = NULL;
            // nextItem->origin = (void *) nextEntry - (void *) inHpi->directoryBuff;
            nextItem->origin = (void *) (
                inDir->firstFilePtr + (count * sizeof(Entry_2))
            );

            if (currentItem->firstChild == NULL) {
                currentItem->firstChild = nextItem;
            }

            if (lastItem != NULL) {
                lastItem->nextSibling = nextItem;
            }

            lastItem = nextItem;
        }
    }
}

uint32_t check_calc_2(uint32_t *cs, byte *buff, uint32_t size)
{
    uint32_t c;
    byte *check = (byte *) cs;

    for (uint32_t count = 0; count < size; count++) {
        c = buff[count];
        check[0] += c;
        check[1] ^= c;
        check[2] += (c ^ ((byte) (count & 0x000000FF)));
        check[3] ^= (c + ((byte) (count & 0x000000FF)));
    }

    return *cs;
}

uint32_t calc_tree_size_2(HPI_Item_2 *item, uint32_t curSum, Count_Mode mode)
{
    if (item->firstChild != NULL) {
        curSum = calc_tree_size_2(item->firstChild, curSum, mode);
    }

    if (item->nextSibling != NULL) {
        curSum = calc_tree_size_2(item->nextSibling, curSum, mode);
    }

    switch (mode) {
        case COUNT_ITENS_ANY:
            curSum++;
            break;
        case COUNT_ITENS_ENTRY:
            if (!item->isDirectory) curSum++;
            break;
        case COUNT_ITENS_DIR:
            if (item->isDirectory) curSum++;
            break;
        case COUNT_BYTES_ANY:
            // testlog("%scurSum (%u) += %u", KMAG, curSum,
                // item->isDirectory ? sizeof(Directory_2) : sizeof(Entry_2));
            curSum += item->isDirectory ? sizeof(Directory_2) : sizeof(Entry_2);
            // testlog(" -> %u [%s]%s\n", curSum, item->name, KNRM);
            break;
        case COUNT_BYTES_ENTRY:
            if (!item->isDirectory) curSum += sizeof(Entry_2);
            break;
        case COUNT_BYTES_DIR:
            if (item->isDirectory) curSum += sizeof(Directory_2);
            break;
    }

    return curSum;
}

uint32_t extract_to_mem_2(FILE *inFile, HPI_Item_2 *inItem, byte **outBufferPtr)
{
    if (inItem->isDirectory) {
        // TODO can't extract directory
    }
    else {
        uint32_t checksum = 0;
        Entry_2 *entry = inItem->entry;

        *outBufferPtr = malloc(entry->decompressedSize + 1);
        (*outBufferPtr)[entry->decompressedSize] = 0;

        fseek(inFile, entry->dataStartPtr, SEEK_SET);

        if (entry->compressedSize) {
            uint32_t outSize = 0;

            while(outSize < entry->decompressedSize) {
                Chunk nextChunk;
                fread(&nextChunk, sizeof(Chunk), 1, inFile);

                byte *sqsh = malloc(nextChunk.compressedSize);

                fread(sqsh, nextChunk.compressedSize, 1, inFile);
                uint32_t nextSize = decompress(sqsh, &nextChunk, (*outBufferPtr) + outSize);

                if (nextSize != nextChunk.decompressedSize) {
                    // TODO - return an error!
                    testlog("Decmp Error! Expected: %u Got: %u\n", nextChunk.decompressedSize, nextSize);
                    break;
                }
                else {
                    outSize += nextSize;
                }

                check_calc_2(&checksum, *outBufferPtr, nextSize);
                free(sqsh);
            }
        }
        else {
            fread(*outBufferPtr, entry->decompressedSize, 1, inFile);
            check_calc_2(&checksum, *outBufferPtr, entry->decompressedSize);
        }

        if (entry->checksum != checksum) {
            testlog("Checksum mismatch! Expected: %u Got: %u\n", entry->checksum, checksum);
        }

        return entry->decompressedSize + 1;
    }

    return 0;
}

void free_tree_2(HPI_Item_2 *item)
{
    if (item->firstChild != NULL) {
        free_tree_2(item->firstChild);
    }

    if (item->nextSibling != NULL) {
        free_tree_2(item->nextSibling);
    }

    // testlog("%sFreeing %s0x%02x%s or %s%u%s %s%s\n",
        // KMAG, KRED, item->path, KMAG, KRED, item->path, KMAG, item->path, KNRM);
    free(item->name);
    free(item->path);
    free(item);
}

int count_children_2(HPI_Item_2 *item)
{
    if (item->firstChild == NULL)
        return 0;

    return count_siblings_2(item->firstChild) + 1;
}

int count_siblings_2(HPI_Item_2 *item)
{
    return item->nextSibling == NULL ? 0 : count_siblings_2(item->nextSibling) + 1;
}

void print_tree_2(HPI_Item_2 *item)
{
    // testlog("%s\n", item->path);
    debug_print_item(item);

    /*testlog("%s  (↓ %s\t→ %s)\n", item->path,
        item->firstChild ? item->firstChild->name : "-",
        item->nextSibling ? item->nextSibling->name : "-");*/

    if (item->firstChild != NULL) {
        print_tree_2(item->firstChild);
    }

    if (item->nextSibling != NULL) {
        print_tree_2(item->nextSibling);
    }
}

// first param may accept * (multiple characters) and ? (one character)
// ex: a?a* matches 'aramon' but not 'arramon'
bool morestrcmp(const char *first, const char *second, bool ignoreCase)
{
    char lower1 = *first;
    char lower2 = *second;

    if (ignoreCase) {
        if (lower1 >= 'A' && lower1 <= 'Z')
            lower1 = 'a' + (lower1 - 'A');

        if (lower2 >= 'A' && lower2 <= 'Z')
            lower2 = 'a' + (lower2 - 'A');
    }

    if (lower1 == '\0' && lower2 == '\0')
        return true;

    if (lower1 == '*' && *(first+1) != '\0' && lower2 == '\0')
        return false;

    if (lower1 == '?' || lower1 == lower2)
        return morestrcmp(first + 1, second + 1, ignoreCase);

    if (lower1 == '*')
        return morestrcmp(first + 1, second, ignoreCase) || morestrcmp(first, second + 1, ignoreCase);

    return false;
}

// private
HPI_Item_2 *find_item_2_sub(HPI_Item_2 *rootItem, char **pathPoints, int pathCount, bool ignoreCase)
{
    char *next = pathPoints[0];
    bool autoPass = strlen(next) == 0; // "" empty string; this is black magic btw

    if (autoPass || morestrcmp(next, rootItem->name, ignoreCase)) {
        if (pathCount == 1) {
            return rootItem;
        }
        else {
            HPI_Item_2 *child = rootItem->firstChild;
            if (child != NULL) {
                HPI_Item_2 *search = find_item_2_sub(child, pathPoints + 1, pathCount - 1, ignoreCase);

                if (search != NULL)
                    return search;
            }
        }
    }

    HPI_Item_2 *sibling = rootItem->nextSibling;
    if (sibling != NULL && pathCount > 0) {
        HPI_Item_2 *search = find_item_2_sub(sibling, pathPoints, pathCount, ignoreCase);

        if (search != NULL)
            return search;
    }

    return NULL;
}

HPI_Item_2 *find_item_2(HPI_Item_2 *rootItem, const char *itemPathSearch, bool ignoreCase)
{
    HPI_Item_2 *result = NULL;
    int len = strlen(itemPathSearch);
    char *pathList = malloc(len * sizeof(char) + 1); // + 1 for null-terminator
    int pathCount = 1;

    strcpy(pathList, itemPathSearch);

    for (int i = 0; i < len; i++) {
        if (pathList[i] == '/') {
            pathList[i] = '\0';
            pathCount++;
        }
    }

    char **pathPoints = malloc(sizeof(char *) * pathCount);

    int last = 0;
    for (int i = 0; i < pathCount; i++) {
        char *next = pathList + last;
        last += strlen(next) + 1;

        pathPoints[i] = next;
    }

    result = find_item_2_sub(rootItem, pathPoints, pathCount, ignoreCase);
    free(pathPoints);

    free(pathList);
    return result;
}

// private
HPI_Item_2 *find_item_anywhere_2_sub(HPI_Item_2 *rootItem, const char* itemName, bool ignoreCase)
{
    if (morestrcmp(itemName, rootItem->name, ignoreCase))
        return rootItem;

    if (rootItem->nextSibling != NULL) {
        HPI_Item_2 *search = find_item_anywhere_2_sub(rootItem->nextSibling, itemName, ignoreCase);

        if (search != NULL)
            return search;
    }

    if (rootItem->firstChild != NULL) {
        HPI_Item_2 *search = find_item_anywhere_2_sub(rootItem->firstChild, itemName, ignoreCase);

        if (search != NULL)
            return search;
    }

    return NULL;
}

HPI_Item_2 *find_item_anywhere_2(HPI_Item_2 *rootItem, const char* itemName, bool ignoreCase)
{
    if (rootItem->firstChild == NULL)
        return NULL;

    return find_item_anywhere_2_sub(rootItem->firstChild, itemName, ignoreCase);
}

// private
HPI_Item_Node_2 *find_item_list_2_sub(HPI_Item_List_2 *list, HPI_Item_Node_2 *currentNode,
    HPI_Item_2 *rootItem, char **pathPoints, int pathCount, bool ignoreCase)
{
    char *next = pathPoints[0];

    if (morestrcmp(next, rootItem->name, ignoreCase)) {
        if (pathCount == 1) {
            list->length++;

            if (currentNode != NULL) {
                HPI_Item_Node_2 *nextNode = malloc(sizeof(HPI_Item_Node_2));
                currentNode->nextNode = nextNode;
                currentNode = nextNode;
            }
            else {
                currentNode = malloc(sizeof(HPI_Item_Node_2));
                list->startNode = currentNode;
            }

            currentNode->nextNode = NULL;
            currentNode->item = rootItem;
        }
        else {
            HPI_Item_2 *child = rootItem->firstChild;
            if (child != NULL) {
                HPI_Item_Node_2 *search = find_item_list_2_sub(list, currentNode,
                    child, pathPoints + 1, pathCount - 1, ignoreCase);

                if (search != NULL)
                    return search;
            }
        }
    }

    HPI_Item_2 *sibling = rootItem->nextSibling;
    if (sibling != NULL && pathCount > 0) {
        HPI_Item_Node_2 *search = find_item_list_2_sub(list, currentNode,
            sibling, pathPoints, pathCount, ignoreCase);

        if (search != NULL)
            return search;
    }

    return currentNode;
}

HPI_Item_List_2 *find_item_list_2(HPI_Item_2 *rootItem, const char* itemPathSearch, bool ignoreCase)
{
    HPI_Item_List_2 *currentList = malloc(sizeof(HPI_Item_List_2));
    currentList->length = 0;
    currentList->startNode = NULL;

    int len = strlen(itemPathSearch);
    char *pathList = malloc(len * sizeof(char));
    int pathCount = 0;

    strcpy(pathList, itemPathSearch);

    for (int i = 0; i < len; i++) {
        if (pathList[i] == '/') {
            pathList[i] = '\0';
            pathCount++;
        }
    }

    if (pathCount > 0) {
        pathCount++; // get the last part of the split

        char **pathPoints = malloc(sizeof(char *) * pathCount);

        int last = 0;
        for (int i = 0; i < pathCount; i++) {
            char *next = pathList + last;
            last += strlen(next) + 1;

            pathPoints[i] = next;
        }

        find_item_list_2_sub(currentList, NULL, rootItem, pathPoints, pathCount, ignoreCase);
        free(pathPoints);
    }

    free(pathList);

    return currentList;
}

// private
HPI_Item_Node_2 *find_item_list_anywhere_2_sub(HPI_Item_List_2 *list, HPI_Item_Node_2 *currentNode,
    HPI_Item_2 *currentItem, const char* itemName, bool ignoreCase)
{
    if (morestrcmp(itemName, currentItem->name, ignoreCase)) {
        list->length++;

        if (currentNode != NULL) {
            HPI_Item_Node_2 *nextNode = malloc(sizeof(HPI_Item_Node_2));
            currentNode->nextNode = nextNode;
            currentNode = nextNode;
        }
        else {
            currentNode = malloc(sizeof(HPI_Item_Node_2));
            list->startNode = currentNode;
        }

        currentNode->nextNode = NULL;
        currentNode->item = currentItem;
    }

    if (currentItem->nextSibling != NULL) {
        HPI_Item_Node_2 *search = find_item_list_anywhere_2_sub(list, currentNode, currentItem->nextSibling, itemName, ignoreCase);

        if (search != NULL)
            currentNode = search;
    }

    if (currentItem->firstChild != NULL) {
        HPI_Item_Node_2 *search = find_item_list_anywhere_2_sub(list, currentNode, currentItem->firstChild, itemName, ignoreCase);

        if (search != NULL)
            currentNode = search;
    }

    return currentNode;
}

HPI_Item_List_2 *find_item_list_anywhere_2(HPI_Item_2 *rootItem, const char* itemName, bool ignoreCase)
{
    if (rootItem->firstChild == NULL)
        return NULL;

    HPI_Item_List_2 *currentList = malloc(sizeof(HPI_Item_List_2));
    currentList->length = 0;
    currentList->startNode = NULL;

    find_item_list_anywhere_2_sub(currentList, NULL, rootItem->firstChild,
        itemName, ignoreCase);

    return currentList;
}

void free_item_list_2(HPI_Item_List_2 *list)
{
    HPI_Item_Node_2 *node = list->startNode;

    while (node != NULL) {
        free(node);

        node = node->nextNode;
    }

    free(list);
}

void write_buffer(const char *filePath, byte *buffer, uint32_t len)
{
    FILE *file = fopen(filePath, "wb");

    fwrite(buffer, len, 1, file);

    fclose(file);
}
