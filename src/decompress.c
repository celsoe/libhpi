#include "decompress.h"
#include "logger.h"
#include "zlib.h"

#define FALSE 0

uint32_t decompress(byte *inBuffer, Chunk *inChunk, byte *outBuffer)
{
    uint32_t checksum = 0;

    for (uint32_t x = 0; x < inChunk->compressedSize; x++) {
        checksum += inBuffer[x];

        if (inChunk->isEncrypted)
            inBuffer[x] = (inBuffer[x] - x) ^ x;
    }

    if (inChunk->checksum != checksum) {
        // TODO - error
        testlog("Checksum mismatch! Expected: %u Got: %u\n", inChunk->checksum, checksum);
    }

    switch (inChunk->compMethod) {
        case 1 : return decompressLZ77(inBuffer, inChunk, outBuffer);
        case 2 : return decompressZLib(inBuffer, inChunk, outBuffer);
        default : return 0;
    }
}

uint32_t decompressLZ77(byte *inBuffer, Chunk *inChunk, byte *outBuffer)
{
    int x;
    int work1;
    int work2;
    int work3;
    int inptr;
    int outptr;
    int count;
    int done;
    char DBuff[4096];
    int DPtr;

    done = FALSE;

    inptr = 0;
    outptr = 0;
    work1 = 1;
    work2 = 1;
    work3 = inBuffer[inptr++];

    while (!done) {
        if ((work2 & work3) == 0) {
            outBuffer[outptr++] = inBuffer[inptr];
            DBuff[work1] = inBuffer[inptr];
            work1 = (work1 + 1) & 0xFFF;
            inptr++;
        }
        else {
            count = *((unsigned short *) (inBuffer+inptr));
            inptr += 2;
            DPtr = count >> 4;
            if (DPtr == 0) {
                return outptr;
            }
            else {
                count = (count & 0x0f) + 2;
                if (count >= 0) {
                    for (x = 0; x < count; x++) {
                        outBuffer[outptr++] = DBuff[DPtr];
                        DBuff[work1] = DBuff[DPtr];
                        DPtr = (DPtr + 1) & 0xFFF;
                        work1 = (work1 + 1) & 0xFFF;
                    }

                }
            }
        }
        work2 *= 2;
        if (work2 & 0x0100) {
            work2 = 1;
            work3 = inBuffer[inptr++];
        }
    }

    return outptr;
}

uint32_t decompressZLib(byte *inBuffer, Chunk *inChunk, byte *outBuffer)
{
    z_stream zs;
    int result;

    zs.next_in = inBuffer;
    zs.avail_in = inChunk->compressedSize;
    zs.total_in = 0;

    zs.next_out = outBuffer;
    zs.avail_out = inChunk->decompressedSize;
    zs.total_out = 0;

    zs.msg = NULL;
    zs.state = NULL;
    zs.zalloc = Z_NULL;
    zs.zfree = Z_NULL;
    zs.opaque = NULL;

    zs.data_type = Z_BINARY;
    zs.adler = 0;
    zs.reserved = 0;

    result = inflateInit(&zs);
    if (result != Z_OK) {
        testlog("Error on inflateInit %d\nMessage: %s\n", result, zs.msg);
        return 0;
    }

    result = inflate(&zs, Z_FINISH);
    if (result != Z_STREAM_END) {
        testlog("Error on inflate %d\nMessage: %s\n", result, zs.msg);
        zs.total_out = 0;
    }

    result = inflateEnd(&zs);
    if (result != Z_OK) {
        testlog("Error on inflateEnd %d\nMessage: %s\n", result, zs.msg);
        return 0;
    }

    return zs.total_out;
}
