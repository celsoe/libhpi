#ifndef LIBHPI_PACKER_H
#define LIBHPI_PACKER_H

#include "libhpi.h"

// orders the sections of the hpi, into the variables
// first, second, third (ex: 'F', 'N', 'D')
void check_order_2(HPI_2 *hpi, char *first, char *second, char *third);

// returns 0 if order is correct, -1 if it's wrong
int assert_order_2(HPI_2 *hpi, char x1, char x2, char x3);

// rebuilds hpi->directoryBuff based on its HPI_Item contents, then overwrites it
// returns the difference between the new dir buffer length, and the old one
int32_t rebuild_dir_buffer_2(HPI_2 *hpi);

// name must be null terminated, parent must be a directory item
void add_file_2(HPI_2 *hpi, char *data, uint32_t dataSize, char *name, HPI_Item_2 *parent);

// name must be null terminated, parent must be a directory item
HPI_Item_2 *make_dir_2(HPI_2 *hpi, char *name, HPI_Item_2 *parent);

// deletes the file data and its entry node from the hpi. returns true if successful
bool delete_file_2(HPI_2 *hpi, HPI_Item_2 *fileItem);

// deletes a directory from the hpi by removing its node from the dir tree
// this operation should only be called on EMPTY directories, otherwise, its children
// only be hidden from the tree, but their data will still exist even if unnaccessible,
// which may cause unexpected behaviors on further manipulation of the hpi (or not)
bool delete_dir_2(HPI_2 *hpi, HPI_Item_2 *dirItem);

// if item is a file, this function simply calls #delete_file_2, otherwise, it will
// automatically traverse the children to delete them from the bottom to the top,
// to correctly call #delete_dir_2 on all directories found, all the way up to the *item
bool delete_auto_2(HPI_2 *hpi, HPI_Item_2 *item);

void rename_2(HPI_2 *hpi, HPI_Item_2 *item, char *name);

// overwrites the item with a different file content
void update_file_2(HPI_2 *hpi, char *data, uint32_t dataSize, HPI_Item_2 *item);

// moves an item into a different parent by changing the dirs tree
void move_item_2(HPI_2 *hpi, HPI_Item_2 *item, HPI_Item_2 *newParent);

// if no compression happens (not needed etc), simply returns 0
// if compression happens, populates *outBlockPtr with compressed data
// ... and *outBlockChunkPtr with the generated chunk to go with the compressed file
// ... and returns the compressed size which is > 0
uint32_t compress_file_2(char *data, uint32_t dataSize, Entry_2 *ioEntry,
                        byte **outBlockPtr, Chunk **outBlockChunkPtr);

#endif
