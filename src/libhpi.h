#ifndef LIBHPI_LIBHPI_H
#define LIBHPI_LIBHPI_H

#include <stdio.h>
#include <stdbool.h>
#include "hpi_structs.h"

#define byte uint8_t

typedef struct HPI_Item_2 {
    char        *name;
    char        *path;
    uint32_t    size;

    bool        isDirectory;
    Directory_2 *dir;       // can only be used if isDirectory
    Entry_2     *entry;     // can only be used if isDirectory == FALSE

    struct HPI_Item_2  *parent;
    struct HPI_Item_2  *nextSibling;
    struct HPI_Item_2  *firstChild; // only if isDirectory

    // ptr in the file to where the dir or entry was taken from
    // this pointer is relative to the HPI's directoryBuff
    // so to get the absolute pos in the file, you need hpi->directoryBuff + origin <WRONG
    void        *origin;
    byte*       cache; // TODO cache of the origin file (from packer etc)
} HPI_Item_2;

typedef struct HPI_2 {
    Header_2    header; // TODO < make this a pointer

    byte        *directoryBuff;
    byte        *namesBuff;

    // difference between the 2 below and header.*BlockLen is that these 2 are the uncompressed lengths
    uint32_t    dirBuffLen;
    uint32_t    namesBuffLen;

    HPI_Item_2  *rootItem;

    // file handle for I/O
    FILE *file;
} HPI_2;

///// [ utility only (used for searching and retrieving a list of results)
    // shall never be created manually, this is done by the find_... functons
    typedef struct HPI_Item_Node_2 {
        HPI_Item_2      *item;
        struct HPI_Item_Node_2 *nextNode;
    } HPI_Item_Node_2;

    // needs to be manually freed with free_item_list_2
    typedef struct HPI_Item_List_2 {
        HPI_Item_Node_2 *startNode; // NULL means this is the last on the list
        uint32_t length;
    } HPI_Item_List_2;
///// ] end utility structs

// BOTH VERSIONS ====================================================

// loads the file and the Version struct from the file into the HPI_Object outHpi
// if the version read is VERSION_TA I need to create an HPI_1
// if the version read is VERSION_TAK I need to create an HPI_2 then load with load_header_2
void load_version(FILE *inFile, Version *outVersion);

// loads a buffer for a given block (e.g for directories and names blocks)
// called internally from the lib but can be usually manually to build an HPI_X by hand
// whoever calls this function is responsible for freeing *outBufferPtr
// returns the size of the buffer loaded
uint32_t load_buffer(FILE *inFile, byte **outBufferPtr, uint32_t blockPtr, uint32_t blockLen);

// TA:K =============================================================

// loads everything automatically into a HPI_2
void load_2(FILE *inFile, HPI_2 *outHpi);

// cleans what was loaded with load_2 automatically (must be called to free the memory etc)
void clean_2(HPI_2 *hpi);

// extracts a given inItem into *outBufferPtr, returns the size of the buffer
// *outBufferPtr should be an empty, UNALOCCATED buffer (ex: byte *buf; &buf)
// whoever calls this function is responsible for freeing *outBufferPtr
uint32_t extract_to_mem_2(FILE *inFile, HPI_Item_2 *inItem, byte **outBufferPtr);

// adds the item inItem as a child of dirItem
// void add_to_directory(HPI_Item_2 *inItem, HPI_Item_2 *dirItem);

void free_tree_2(HPI_Item_2 *item);

// Manual steps for loading part by part ----------------------------

void load_header_2(FILE *inFile, Header_2 *inHeader);

void process_directory_2(HPI_2 *inHpi, HPI_Item_2 *currentItem, char *currentPath);

void process_entries_2(HPI_2 *inHpi, HPI_Item_2 *currentItem, char *currentPath);

// Other ------------------------------------------------------------

// checksum operation
uint32_t check_calc_2(uint32_t *outChecksum, byte *buff, uint32_t size);

typedef enum {COUNT_ITENS_ANY, COUNT_ITENS_ENTRY, COUNT_ITENS_DIR,
                COUNT_BYTES_ANY, COUNT_BYTES_ENTRY, COUNT_BYTES_DIR} Count_Mode;
uint32_t calc_tree_size_2(HPI_Item_2 *item, uint32_t curSum, Count_Mode mode);

// Utility functions ------------------------------------------------

void debug_print_item(HPI_Item_2 *item);
void debug_print_hpi(HPI_2 *hpi);

int count_children_2(HPI_Item_2 *item);
int count_siblings_2(HPI_Item_2 *item);

// printf's the whole directory/file tree starting at root item
void print_tree_2(HPI_Item_2 *item);

// match string supports "*" and "?" for searching
bool morestrcmp(const char *match, const char *str, bool ignoreCase);

// finds an item recursively starting at rootItem based on a path (ex: "/anims/BuildPic/vermer.jpg")
HPI_Item_2 *find_item_2(HPI_Item_2 *rootItem, const char* itemPathSearch, bool ignoreCase);

// finds an item recursively on every subdirectory of rootItem given a file name (ex: "vermer.jpg")
// unlike find_item_2, this function doesn't care about the path structure of the item, and thus
// specifying a path is erroneous
HPI_Item_2 *find_item_anywhere_2(HPI_Item_2 *rootItem, const char* itemName, bool ignoreCase);

// same as find_item_2 but return a list of items that matched
// the list that is returned MUST be fred by calling free_item_list_2
HPI_Item_List_2 *find_item_list_2(HPI_Item_2 *rootItem, const char* itemPathSearch, bool ignoreCase);

// same as find_item_anywhere_2 but return a list of items that matched
// the list that is returned MUST be fred by calling free_item_list_2
HPI_Item_List_2 *find_item_list_anywhere_2(HPI_Item_2 *rootItem, const char* itemName, bool ignoreCase);

// frees the list object (and all neighboor nodes), not the actual HPI_Items that the list contains
// the HPI_Items should be fred with free_tree_2
void free_item_list_2(HPI_Item_List_2 *list);

// dump a buffer to an output a file
void write_buffer(const char *filePath, byte *buffer, uint32_t len);

// TA ===============================================================

#endif //LIBHPI_LIBHPI_H
