#ifndef LIBHPI_HPI_STRUCTS_H
#define LIBHPI_HPI_STRUCTS_H

#include <stdint.h>

#define HAPI_MARKER     0x49504148
#define VERSION_TA      0x00010000
#define VERSION_TAK     0x00020000
#define SQSH_MARKER     0x48535153

// ANY VERSION ==============

typedef struct Version {
    uint32_t     marker;
    uint32_t     version;
} Version;

#pragma pack(1)
typedef struct Chunk {
    uint32_t    marker;
    uint8_t     unknown1;
    uint8_t     compMethod;
    uint8_t     isEncrypted;
    uint32_t    compressedSize;
    uint32_t    decompressedSize;
    uint32_t    checksum;
} Chunk;
#pragma pack()

// TA:K VERSION =============

typedef struct Header_2 {
    uint32_t    dirBlockPtr;
    uint32_t    dirBlockLen;
    uint32_t    namesBlockPtr;
    uint32_t    namesBlockLen;
    uint32_t    data;
    uint32_t    last78;
} Header_2;

typedef struct Directory_2 {
    uint32_t    namePtr;        // relative to the namesBlock
    uint32_t    firstSubDirPtr; // relative to the dirBlock
    uint32_t    subDirCount;
    uint32_t    firstFilePtr;   // relative to the dirBlock
    uint32_t    fileCount;
} Directory_2;

typedef struct Entry_2 {
    uint32_t    namePtr;        // relative to the namesBlock
    uint32_t    dataStartPtr;
    uint32_t    decompressedSize;
    uint32_t    compressedSize;     // 0 = no compression
    uint32_t    date;               // in time_t format
    uint32_t    checksum;
} Entry_2;

#endif //LIBHPI_HPI_STRUCTS_H
